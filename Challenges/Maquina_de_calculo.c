//Ivan Lazcano Sindin
#include <stdio.h>

int main()
{
    int a, b;
    char operacion;
    int count;
    scanf("%d", &count);
    while(count--)
    {
        scanf("%d", &b);
        scanf("%s", &operacion);
        scanf("%d", &a);
        switch(operacion)
        {
            case '+':printf("%d\n", a+b);break;
            case '*':printf("%d\n", a*b);break;
            case '/':if(!a)printf("ERROR\n");else printf("%d\n", b/a);break;
            case '-':printf("%d\n", b-a);break;
        }
    }
}
