#include <iostream>
using namespace std;

int main()
{
    int rows, input;
    cin >> rows;
    bool identidad = true;
    while(rows)
    {
        identidad = true;
        
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                cin >> input;
                if  ( (i == j && input != 1) || (i != j && input != 0) )
                    identidad = false;
            }
        }
        
        if(identidad)
            printf("SI\n");
        else
            printf("NO\n");
        
        cin >> rows;
    }
    return 0;
}
