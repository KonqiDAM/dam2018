#include <iostream>
#include <cstdio>
#define gc getchar_unlocked

void scanint(int &x)
{
    register int c = gc();
    x = 0;
    for(;(c<48 || c>57);c = gc());
    for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
}

int main()
{
    int rows;
    int number;
    bool triangular = true;
    scanint(rows);
    while(rows)
    {
        int matrix[rows][rows];
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                scanint(number);
                matrix[i][j] = number;
            }
            
        }
        
        for (int i = 0; i < rows-1; i++)
        {
            for (int j = i+1; j < rows; j++)
            {
                if(matrix[i][j] != 0)
                {
                    triangular = false;
                    break;
                }
                
            }
            if(!triangular)
                break;
        }
        if(triangular)
            printf("SI\n");
        else
        {
            triangular = true;
            for (int i = rows-1; i > 0; i--)
            {
                for (int j = i-1; j >=  0; j--)
                {
                    if(matrix[i][j] != 0)
                    {
                        triangular = false;
                        break;
                    }
                    
                }
                if(triangular == false)
                    break;
            }
            printf(triangular ? "SI\n" : "NO\n");
        }
        scanint(rows);
        triangular = true;
    }
    
    return 0x0;
}
