#include <iostream>
using namespace std;

int main()
{
    int rows;
    int number;
    bool triangular = true;
    cin >> rows;
    while(rows)
    {
        int matrix[rows][rows];
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                cin >> number;
                matrix[i][j] = number;
            }
            
        }
        
        for (int i = 0; i < rows-1; i++)
        {
            for (int j = i+1; j < rows; j++)
            {
                if(matrix[i][j] != 0)
                {
                    triangular = false;
                    break;
                }
                
            }
            if(!triangular)
                break;
        }
        if(triangular)
            cout << "SI\n";
        else
        {
            triangular = true;
            for (int i = rows-1; i > 0; i--)
            {
                for (int j = i-1; j >=  0; j--)
                {
                    if(matrix[i][j] != 0)
                    {
                        triangular = false;
                        break;
                    }
                    
                }
                if(triangular == false)
                    break;
            }
            if(triangular)
                cout << "SI\n";
            else 
                cout << "NO\n";
        }
        cin >> rows;
        triangular = true;
    }
    
    return 0x0;
}
