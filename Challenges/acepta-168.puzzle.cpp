#include <iostream>
using namespace std;

int main()
{
    int count;
    int piece;
    do
    {
        cin >> count;
        bool pieces[count+1];
        for (int i = 1; i < count+1; i++)
        {
            pieces[i] = false;
        }
        
        for (int i = 1; i < count; i++)
        {
            cin >> piece;
            pieces[piece] = true;
        }
        
        for (int i = 1; i < count+1; i++)
        {
            if( pieces[i] == false )
            {
                cout << i << endl;
                break;
            }
        }
    }while(count);
    
    return 0x0;
}
