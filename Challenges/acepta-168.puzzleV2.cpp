#include <iostream>
using namespace std;

int main()
{
    int count;
    int sum;
    int temporal;
    int total;
    do
    {
        cin >> count;
        sum = 0;
        if(count)
        {
            for (int i = 0; i < count-1; i++)
            {
                cin >> temporal;
                sum += temporal;
            }
            
            total = (count * ( count + 1 )) / 2;
            cout << total - sum << endl;
        }
        
    }while(count);
    
    return 0x0;
}
