#include <iostream>
#include <cstdio>
#define gc getchar_unlocked

void scanint(int &x)
{
    register int c = gc();
    x = 0;
    for(;(c<48 || c>57);c = gc());
    for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
}

int main()
{
    int count;
    int sum;
    int temporal;
    int total;
    do
    {
        scanint(count);
        sum = 0;
        if(count)
        {
            for (int i = 0; i < count-1; i++)
            {
                scanint(temporal);
                sum += temporal;
            }
            
            total = (count * ( count + 1 )) / 2;
            printf("%d\n", total-sum);
        }
        
    }while(count);
    
    return 0x0;
}
