#include <iostream>
#include <cstdio>
#define gc getchar_unlocked

void scanint(int &x)
{
    register int c = gc();
    x = 0;
    for(;(c<48 || c>57);c = gc());
    for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
}

int main()
{
    int count;
    int piece;
    do
    {
        scanint(count);
        if(count)
        {
            bool pieces[count+1];
            for (int i = 1; i < count+1; i++)
            {
                pieces[i] = false;
            }
            
            for (int i = 1; i < count; i++)
            {
                scanint(piece);
                pieces[piece] = true;
            }
            
            for (int i = 1; i < count+1; i++)
            {
                if( pieces[i] == false )
                {
                    printf("%d\n", i);
                    break;
                }
            }
        }
    }while(count);
    
    return 0x0;
}
