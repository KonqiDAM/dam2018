#include <stdio.h>

int main()
{
    int suma = 0;
    int first = 1;
    while(1)
    {
        char c = getchar_unlocked();
        if(c == '-')
            return 0;
        else if(c == '\n')
        {
            printf(" = %d\n", suma);
            suma = 0;
            first = 1;
        }
        else
        {
            if(first)
            {
                printf("%d", c-48);
                first = 0;
            }
            else
                printf(" + %d", c-48);
            suma += c-48;
        }
    }
}
