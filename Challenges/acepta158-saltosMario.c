//Ivan Lazcano Sindin
#include <stdio.h>

int main (void)
{
    int subir, bajar, alturaAct, alturaNueva;
    
    int i, j;
    scanf("%d", &i);
    while(i--)
    {
        subir = bajar = 0;
        scanf("%d", &j);
        scanf("%d", &alturaAct);
        j--;
        while(j--)
        {
            scanf("%d", &alturaNueva);
            if(alturaNueva > alturaAct)
                subir++;
            else if(alturaNueva != alturaAct)
                bajar++;
            alturaAct = alturaNueva;
        }
        printf("%d %d\n",subir, bajar);
    }
}
