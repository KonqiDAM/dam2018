//Ivan Lazcano Sindin
using System;
class MainClass
{
    public static void Main(string[] args)
    {
        int subir, bajar, i, j;
        string[] entrada;

        i = Convert.ToInt32(Console.ReadLine());
        while(i-- != 0)
        {
            subir = bajar = 0;
            j = Convert.ToInt32(Console.ReadLine());
            entrada = new string[j];
            entrada = Console.ReadLine().Split();
            for (int k = 0; k < j-1; k++)
            {
                if (Convert.ToInt32(entrada[k]) > Convert.ToInt32(entrada[k + 1]))
                    bajar++;
                else if (Convert.ToInt32(entrada[k]) < Convert.ToInt32(entrada[k + 1]))
                    subir++;
                    
            }
            Console.WriteLine("{0} {1}",subir, bajar);
        }
    }
}
