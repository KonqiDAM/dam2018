#include <stdio.h>

int main()
{
    int ancho, alto, i, j;
    int minas = 0;
    int totalCon6;
    scanf("%d", &ancho);
    scanf("%d", &alto);
    if(alto == 0 || ancho == 0)
        return 0;
    while(1)
    {
        totalCon6 = 0;
        getchar_unlocked();//leer salto de linea
        char mapa[alto][ancho];
        for (i = 0; i < alto; i++)
        {
            for (j = 0; j < ancho; j++)
            {
                mapa[i][j] = getchar_unlocked();
            }
            getchar_unlocked();//leer salto de linea
        }
        
        for (i = 1; i < alto-1; i++)
        {
            for (j = 1; j < ancho-1; j++)
            {
                if(mapa[i][j] == '-')
                {
                    minas = 0;
                    if(mapa[i][j+1] == '*')
                        minas++;
                    if(mapa[i][j-1] == '*')
                        minas++;
                    if(mapa[i+1][j] == '*')
                        minas++;
                    if(mapa[i-1][j] == '*')
                        minas++;
                    if(mapa[i+1][j+1] == '*')
                        minas++;
                    if(mapa[i-1][j+1] == '*')
                        minas++;
                    if(mapa[i+1][j-1] == '*')
                        minas++;
                    if(mapa[i-1][j-1] == '*')
                        minas++;
                    if(minas >= 6)
                        totalCon6++;
                }
            }
        }
        printf("%d\n", totalCon6);
        scanf("%d", &ancho);
        scanf("%d", &alto);
        if(alto == 0 || ancho == 0)
            return 0;
    }
}
