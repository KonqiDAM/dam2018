#include <stdio.h>

void quicksort(int array[], int start, int end)
{
 int pivot, left, right;
 if (start < end)
 {
 pivot = array[(start + end) / 2];
 left = start;
 right = end;
 do
 {
 while (array[left] < pivot && left <= end)
 left++;
 while (array[right] > pivot && right >= start)
 right--;
 if (left <= right)
 {
 int aux = array[left];
 array[left] = array[right];
 array[right] = aux;
 left++;
 right--;
 }
 } while (left <= right);
 if (start < right)
 quicksort(array, start, right);
 if (left < end)
 quicksort(array, left, end);
 }
}

int main()
{
    int count, i;
    int burros;
    int sacos;
    int sacosCargados;
    scanf("%d", &count);
    while(count--)
    {
        sacosCargados = 0;
        scanf("%d", &burros);
        scanf("%d", &sacos);
        int pesoSaco[sacos];
        for (i = 0; i < sacos; i++)
        {
            scanf("%d", &pesoSaco[i]);
        }
        quicksort(pesoSaco, 0, sacos-1);
        for (i = 0; i < sacos-1; i++)
        {
            if(pesoSaco[i] == pesoSaco[i+1])
            {
                sacosCargados++;
                if(sacosCargados >= burros)
                    break;
                i++;
            }
        }
        printf("%d\n", sacosCargados);
    }
    return 0;
}
