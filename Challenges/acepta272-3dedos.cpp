#include <stdio.h>
#define gc getchar_unlocked

void scanint(int &x)
{
    register int c = gc();
    x = 0;
    for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
}


void convert10tob(int N)
{
     if (N == 0)
        return;
     int x = N % 6;
     N /= 6;
     if (x < 0)
        N += 1; 
     convert10tob(N);
     printf("%d", x < 0 ? x + (6 * -1) : x);
     return;
}


int main()
{
    int count;
    int n;
    scanint(count);
    while(count--)
    {
        scanint(n);
        if(n)
        {
            convert10tob(n);
            printf("\n");
        }
        else
            printf("0\n");
    }
    return 0;
}
