#include <iostream>
#include <cstdio>
#define gc getchar_unlocked

void scanint(int &x)
{
    register int c = gc();
    x = 0;
    for(;(c<48 || c>57);c = gc());
    for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
}

int main()
{
    int n;
    int a, b, c, d, e;
    scanint(n);
    while(n--)
    {
        scanint(a);
        scanint(b);
        scanint(c);
        scanint(d);
        scanint(e); 
        
        printf("%d", a*6 + b*8 + c *10 + d*2*e);
        puts("");
    }
    return 0;            
}
