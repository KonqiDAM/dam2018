//Ivan Lazcano Sindin
#include <stdio.h>

int main()
{
    short entrada, salida;
    int count;
    scanf("%d", &count);
    while(count--)
    {
        scanf("%hd", &entrada);
        scanf("%hd", &salida);
        printf(entrada + salida >= 0 ? "SI\n" : "NO\n");
        
    }
}
