#include <stdio.h>
#define gc getchar_unlocked

void scanint(int &x)
{
    register int c = gc();
    x = 0;
    for(;(c<48 || c>57);c = gc());
    for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
}
int main()
{
    int count, altura, altAnterior, i;
    int countPicos = 0;
    short subida = 0, bajada = 0;
    int primeraAltura;
    scanint(count);
    while(count)
    {
        for (i = 0; i < count; i++)
        {
            
            if(i == 0)
            {
                scanint(altAnterior);
                primeraAltura = altAnterior;
                continue;
            }
            scanint(altura);
            
            if(subida && altura < altAnterior)
            {
                countPicos++;
            }
                
            if(altura > altAnterior)
                subida = 1;
            else
                subida = 0;
            if(i == 1 && altura < altAnterior)
                bajada = 1;
            
            altAnterior = altura;
        }
        //printf("b: %d\n", subida);
        if(altAnterior < primeraAltura && bajada)
            countPicos++;
        if(subida && primeraAltura < altAnterior )
                countPicos++;
        printf("%d\n", countPicos);
        bajada = countPicos = subida = 0;
        scanint(count);
    }
}
