#include <stdio.h>
void quicksort(int array[], int start, int end)
{
 int pivot, left, right;
 if (start < end)
 {
 pivot = array[(start + end) / 2];
 left = start;
 right = end;
 do
 {
 while (array[left] < pivot && left <= end)
 left++;
 while (array[right] > pivot && right >= start)
 right--;
 if (left <= right)
 {
 int aux = array[left];
 array[left] = array[right];
 array[right] = aux;
 left++;
 right--;
 }
 } while (left <= right);
 if (start < right)
 quicksort(array, start, right);
 if (left < end)
 quicksort(array, left, end);
 }
}


int main()
{
    int hangares, naves;
    int i;
    int tnave;
    int caben = 1;
    do
    {
        caben = 1;
        scanf("%d", &hangares);
        if(hangares == 0)
            return 0;
            
        int thangar[hangares];
        for (i = 0; i < hangares; i++)
        {
            scanf("%d", &thangar[i]);
            
        }
        quicksort(thangar, 0, hangares-1);
        scanf("%d", &naves);
        for (i = 0; i < naves; i++)
        {
            scanf("%d", &tnave);
            if(caben == 1)
            {
                if(tnave > thangar[hangares-1])
                {
                    caben = 0;
                }
                else
                {
                    thangar[hangares-1]-=tnave;
                    quicksort(thangar, 0, hangares-1);
                }
            }
        }
        if(caben == 1)
            printf("SI\n");
        else
            printf("NO\n");

    }while(1);
    return 0;
}
