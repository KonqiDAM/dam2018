#include <stdio.h>

int main()
{
    int s, b1, b2, count;
    int d1, d2, i1, i2;
    scanf("%d", &count);
    while(count--)
    {
        i1 = i2 = 1;
        scanf("%d", &s);
        scanf("%d", &b1);
        scanf("%d", &b2);
        d1 = s-b1;//distancia a cada bomba
        d2 = s-b2;
        if(d1 < 0)
        {
            i1 = 0;//direccion, si izquierda o derecha
            d1 = -d1;
        }
        if(d2 < 0)
        {
            i2 = 0;
            d2 = -d2;
        }
        if(i1 == i2)//Spdierman esta en un extremo si estan en la misma dir
        {
            if(d1 < d2)
                printf("%d\n", d2);
            else
                printf("%d\n", d1);
        }
        else//spiderman esta en medio, asi que vamos al mas cercano, volvemos 
        {//al punto inicial y vamos al punto mas lejano
            if(d1 < d2)
                printf("%d\n", d1*2+d2);
            else
                printf("%d\n", d2*2+d1);
        }
    }
}
