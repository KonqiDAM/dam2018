#include <stdio.h>
#define gc getchar_unlocked

void scanint(int &x)
{
    register int c = gc();
    x = 0;
    for(;(c<48 || c>57);c = gc());
    for(;c>47 && c<58;c = gc()) x = (x<<1) + (x<<3) + c - 48;
}
int main()
{
    int s, b1, b2, count;
    int d1, d2, i1, i2;
    scanint(count);
    while(count--)
    {
        i1 = i2 = 1;
        scanint(s);
        scanint(b1);
        scanint(b2);
        d1 = s-b1;//distancia a cada bomba
        d2 = s-b2;
        if(d1 < 0)
        {
            i1 = 0;//direccion, si izquierda o derecha
            d1 = -d1;
        }
        if(d2 < 0)
        {
            i2 = 0;
            d2 = -d2;
        }
        if(i1 == i2)//Spdierman esta en un extremo si estan en la misma dir
            if(d1 < d2)
                printf("%d\n", d2);
            else
                printf("%d\n", d1);
        else
            if(d1 < d2)
                printf("%d\n", d1*2+d2);
            else
                printf("%d\n", d2*2+d1);
    }
}
