#include <stdio.h>
#define gc getchar_unlocked

int scanint()
{
    register int c = gc();
    int x = 0;
    for(;c>47 && c<58;c = gc()) 
        x = (x<<1) + (x<<3) + c - 48;
    return x;
}
int main()
{
    int count;
    char s[] = "stanlee";
    char s2[] = "STANLEE";
    int l = 0;
    int encontrado = 0;
    char c;
    count = scanint();
    while(count--)
    {
        c = getchar_unlocked();
        while( c != '\n')
        {
            if(c == s[l] || s2[l] == c)
            {
                l++;
                if(l == 7)
                {
                    l = 0;
                    encontrado++;
                }
            }
            c = getchar_unlocked();
        }
        printf("%d\n", encontrado);
        encontrado = l = 0;
    }
}
