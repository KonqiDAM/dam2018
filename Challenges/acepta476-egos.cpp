#include <stdio.h>
#define gc getchar_unlocked

int compatible(int nivel, int signo, int poder)
{
    if(signo == -1 && nivel > poder)
        return 1;
    else if(signo == 1 && nivel < poder)
        return 1;
    else if(signo == 0 && nivel == poder)
        return 1;
    else
        return 0;
}
void scanint(int &x)
{
    register int c = gc();
    x = 0;
    for(;(c<48 || c>57);c = gc());
    for(;c>47 && c<58;c = gc()) {x = (x<<1) + (x<<3) + c - 48;}
}
int main()
{
    int egos, i, j, count;
    char aux;
    scanint(egos);
    while(egos)
    {
        int poder[egos];
        int signo[egos];
        int nivel[egos];
        int ocupado[egos];
        for (i = 0; i < egos; i++)
        {
            scanint(poder[i]);
            ocupado[i] = 0;
        }
        for (i = 0; i < egos; i++)
        {
            aux = gc();
            if(aux == '<')
                signo[i] = -1;
            else if(aux == '>')
                signo[i] = 1;
            else 
                signo[i] = 0;
                
            nivel[i] = gc()-48;
            gc();
        }
        count = 0;
        for (i = 0; i < egos-1; i++)
        {
            for (j = i+1; j < egos; j++)
            {
                if( !ocupado[i] && !ocupado[j] && compatible(nivel[i], signo[i], poder[j]) 
                   && compatible(nivel[j], signo[j], poder[i]))
                {
                    printf("%d %d\n", i+1, j+1);
                    ocupado[j] = 1;
                    count++;
                    break;
                }
            }
        }
        if(!count)
            printf("NO HAY\n");
        printf("---\n");
        scanint(egos);
    }
}
