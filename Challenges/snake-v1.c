#include <stdio.h>
#include <stdbool.h>

int main()
{
    
    int count;
    int m, n;
    bool right = true;
    bool printed = false;
    
    scanf("%d", &count);
    for(int i = 0; i < count; i++)
    {
        scanf("%d", &m);
        scanf("%d", &n);
        
        for (int row = 0; row < m; row++)
        {
            for (int col = 0; col < n; col++)
            {
                if( row % 2 )
                {
                    if(( col == 0) && (!right) )
                    {
                        printf("#");
                        right = true;
                        printed = true;
                    }
                    else if(col == (n - 1 ) && (right) && (!printed))
                    {
                        printf("#");
                        right = false;
                    }
                    else
                        printf(".");
                    
                }
                else
                {
                    printf("#");
                    printed = false;
                }
            }
            printf("\n");
        }
        
    }
    
    return 0;
}
