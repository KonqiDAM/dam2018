// Ivan Lazcano Sindin
using System;
public class E
{
    public static void Main()
    {
        int i;
        bool escribir = true;
        Console.Write("Numero? ");
        int a = Convert.ToInt32(Console.ReadLine());
        Console.Write("Otro numero? ");
        int b = Convert.ToInt32(Console.ReadLine());

        int mayor = a >= b ? a : b;
        int menor = a <= b ? a : b;
        for ( i = menor; i <= mayor; i++)
        {
            if (escribir)
            {
                if (i == 0)
                {
                    i++;
                    continue;
                }
                Console.Write("{0} ", i);
                escribir = false;
            }
            else
                escribir = true;

        }

        Console.WriteLine();
        for ( i = menor; i <= mayor; i += 2)
        {
            if (i == 0)
                continue;
            Console.Write("{0} ", i);
        }

        Console.WriteLine();
        i = menor;
        while(i <= mayor)
        {
            if (i == 0)
            {
                i += 2;
                continue;
            }
            Console.Write("{0} ", i);
            i += 2;
        }

        Console.WriteLine();
        i = menor;
        do
        {
            if (i == 0)
            {
                i += 2;
                continue;
            }
            Console.Write("{0} ", i);
            i += 2;
        } while (i <= mayor);
    }
}
