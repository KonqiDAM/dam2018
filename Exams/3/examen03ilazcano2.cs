//Ivan Lazcano Sindin
using System;

public class MainClass
{
    public static void Main()
    {
        Console.Write("Introduce una letra: ");
        char letra = Convert.ToChar(Console.ReadLine());

        switch(letra)
        {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':Console.WriteLine("Es una vocal");break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':Console.WriteLine("Es un numero");break;
            case '\"':
            case '\'':Console.WriteLine("Es un delimitador"); break;
            default:Console.WriteLine("Otro"); break;
                
        }

        if( letra == 'a' || letra == 'e' || letra == 'i' 
                || letra == 'o' || letra == 'u')  
            Console.WriteLine("Es una vocal");
        else if( letra >= '0' && letra <= '9')
            Console.WriteLine("Es un numero");
        else if( letra == '\"' || letra == '\'')
            Console.WriteLine("Es un delimitador");
        else
            Console.WriteLine("Otro");
            
    }
}
