//Ivan Lazcano Sindin
using System;

public class MainClass
{
    public static void Main()
    {
        string entrada;
        int sumandos;
        double converge;
        double sumando1, sumando2, sumando3;
        int divisor;
        do
        {
            Console.Write("Sumandos? ");
            entrada = Console.ReadLine();
            if (entrada != "fin")
            {
                sumandos = Convert.ToInt32(entrada);
                if (sumandos <= 0)
                    Console.WriteLine("No se puede sumar menos de 1 "
                                      + "termino");
                else
                {
                    converge = 1;
                    sumando1 = 1;
                    sumando2 = 0.5;
                    sumando3 = 0;
                    divisor = 1;
                    for (int i = 1; i < sumandos; i++)
                    {
                        divisor *= 2;
                        converge += 1.0 / divisor;

                        sumando3 = sumando2;
                        sumando2 = sumando1;
                        sumando1 = 1.0 / divisor;


                    }
                    Console.Write("La suma vale {0}", converge);
                    if (sumandos >= 3)
                        Console.WriteLine(". La media de los 3 ultimos es {0}",
                                      (sumando1 + sumando2 + sumando3) / 3);
                    else
                        Console.WriteLine();
                }

            }

        } while (entrada != "fin");
        Console.WriteLine("Sesion terminada");
    }
}
