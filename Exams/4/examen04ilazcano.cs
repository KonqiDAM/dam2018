//Ivan Lazcano Sindin
using System;

public class Examen1
{
    struct programadores
    {
        public string nombre;
        public string habilidades;
        public ushort anyo;
        public bool plantilla;
        public string comentarios;
    }
    public static void Main()
    {
        int countProgramadores = 0;
        string aux, opcion;
        string aux2;
        const int SIZE = 1000;
        programadores[] p = new programadores[SIZE];

        do
        {
            Console.WriteLine("1. Anotar nuevo programador");
            Console.WriteLine("2. Buscar programadores");
            Console.WriteLine("3. Ver detalles de un programador");
            Console.WriteLine("4. Modificar un dato");
            Console.WriteLine("5. Borrar un registro");
            Console.WriteLine("6. Estadisticas generales");
            Console.WriteLine("7. Estadisticas de habilidad");
            opcion = Console.ReadLine();

            switch (opcion)
            {
                case "1":
                    do
                    {
                        Console.WriteLine("Nombre? ");
                        p[countProgramadores].nombre = Console.ReadLine();
                        if (p[countProgramadores].nombre == "")
                            Console.WriteLine(
                                "Nombre no debe estar vacio!");

                    } while (p[countProgramadores].nombre == "");

                    do
                    {
                        Console.WriteLine("Habilidades? ");
                        p[countProgramadores].habilidades =
                            Console.ReadLine();
                        if (p[countProgramadores].habilidades == "")
                            Console.WriteLine(
                                "Las habilidades no debe estar vacias!");

                    } while (p[countProgramadores].habilidades == "");

                    Console.WriteLine("Año de nacimiento? ");
                    aux = Console.ReadLine();
                    if (aux == "")
                        p[countProgramadores].anyo = 0;
                    else
                        p[countProgramadores].anyo = Convert.ToUInt16(aux);


                    Console.WriteLine("Esta en plantilla? (s/n)");
                    aux = Console.ReadLine().ToLower();
                    p[countProgramadores].plantilla = aux == "s";

                    Console.WriteLine("Comentarios adicionales: ");
                    p[countProgramadores].comentarios = Console.ReadLine();
                    countProgramadores++;

                    for (int i = 0; i < countProgramadores - 1; i++)
                    {
                        for (int j = i + 1; j < countProgramadores; j++)
                        {
                            aux = p[i].nombre + p[i].anyo.ToString();
                            aux2 = p[j].nombre + p[j].anyo.ToString();
                            if (aux.CompareTo(aux2) >= 0)
                            {
                                programadores swap = p[i];
                                p[i] = p[j];
                                p[j] = swap;

                            }

                        }
                    }

                    break;

                case "2"://buscar
                    if (countProgramadores == 0)
                        Console.WriteLine("No hay datos");
                    else
                    {
                        bool nombre = false;
                        Console.WriteLine("Buscar por Nombre o Habilidad? " +
                                          "(n/h): ");
                        aux2 = Console.ReadLine().ToLower();
                        if (aux2 == "n")
                            nombre = true;
                        Console.WriteLine("Termino a buscar: ");
                        aux = Console.ReadLine().ToLower();
                        int contador = 0;
                        for (int i = 0; i < countProgramadores; i++)
                        {
                            if ((nombre
                                 && p[i].nombre.ToLower().Contains(aux))
                                 || (!(nombre)
                                 &&
                                 p[i].habilidades.ToLower().Contains(aux))
                               )
                            {
                                Console.WriteLine("Numero: {0}," +
                                                  " Nombre: {1}," +
                                                  " Habilidades {2}," +
                                                  " Plantilla: "
                                                  , i + 1,
                                                  p[i].nombre,
                                                  p[i].habilidades,
                                                  p[i].plantilla ?
                                                  "P" : "NP"
                                                 );
                                contador++;
                                if (contador % 22 == 21)
                                    Console.ReadLine();
                            }
                        }
                    }
                    break;

                case "3"://ver uno
                    if (countProgramadores == 0)
                        Console.WriteLine("No hay datos");
                    else
                    {
                        Console.WriteLine("Numero de programador a buscar: ");
                        int numero = Convert.ToInt32(Console.ReadLine()) - 1;
                        if (numero < 0 || numero >= countProgramadores)
                        {
                            Console.WriteLine("Error en el numero");
                        }
                        else
                        {
                            Console.WriteLine("Nombre: {0}",
                                              p[numero].nombre);
                            Console.WriteLine("Habilidades: {0}",
                                              p[numero].habilidades);
                            Console.WriteLine("Año: {0}", p[numero].anyo);
                            Console.WriteLine("Plantilla: {0}",
                                              p[numero].plantilla ?
                                              "P" : "NP");
                            Console.WriteLine("Comentarios: {0}",
                                              p[numero].comentarios);
                        }
                    }
                    Console.ReadLine();
                    break;

                case "4"://Modificar
                    bool tieneErrores = false;
                    if (countProgramadores == 0)
                        Console.WriteLine("No hay datos");
                    else
                    {
                        Console.WriteLine("Numero a modificar?");
                        int numero = Convert.ToInt32(Console.ReadLine()) - 1;
                        if (numero < 0 || numero >= countProgramadores)
                        {
                            Console.WriteLine("Error en el numero");
                        }
                        else
                        {
                            Console.WriteLine("Nombre anterior: {0}",
                                              p[numero].nombre);
                            aux = Console.ReadLine();
                            if (aux != "")
                                p[numero].nombre = aux;

                            Console.WriteLine("Habilidades anteriores: {0}",
                                              p[numero].habilidades);
                            aux = Console.ReadLine();
                            if (aux != "")
                                p[numero].habilidades = aux;

                            if (p[numero].habilidades[0] == ' ' ||
                                 p[numero].habilidades
                                 [p[numero].habilidades.Length - 1] == ' '
                                 || p[numero].habilidades.Contains(", "))
                                tieneErrores = true;

                            if (tieneErrores)
                            {
                                Console.WriteLine("El dato tiene errores, " +
                                                  "desea arreglarlo? " +
                                                  "(s/n): ");
                                aux2 = Console.ReadLine().ToLower();
                                if (aux2 == "s")
                                {
                                    p[numero].habilidades =
                                        p[numero].habilidades.Trim();

                                    while (p[numero].habilidades.
                                           Contains(", "))
                                    {
                                        p[numero].habilidades =
                                        p[numero].habilidades.
                                                 Replace(", ", ",");

                                    }
                                }
                            }


                            Console.WriteLine("Año anterior: {0}",
                                              p[numero].anyo);
                            aux = Console.ReadLine();
                            if (aux != "")
                                p[numero].anyo = Convert.ToUInt16(aux);

                            Console.WriteLine("Plantilla: {0}",
                                              p[numero].plantilla ?
                                              "P" : "NP");
                            aux = Console.ReadLine();
                            if (aux != "")
                            {
                                p[countProgramadores].plantilla = aux == "s";
                            }
                            Console.WriteLine("Comentarios: {0}",
                                              p[numero].comentarios);
                        }
                    }

                    break;

                case "5"://borrar
                    if (countProgramadores == 0)
                        Console.WriteLine("No hay datos");
                    else
                    {
                        Console.WriteLine("Numero de programador a borrar: ");
                        int numero = Convert.ToInt32(Console.ReadLine()) - 1;
                        if (numero < 0 || numero >= countProgramadores)
                        {
                            Console.WriteLine("Error en el numero");
                        }
                        else
                        {
                            Console.WriteLine("Nombre: {0}",
                                              p[numero].nombre);
                            Console.WriteLine("Habilidades: {0}",
                                              p[numero].habilidades);
                            Console.WriteLine("Año: {0}", p[numero].anyo);
                            Console.WriteLine("Plantilla: {0}",
                                              p[numero].plantilla ?
                                              "P" : "NP");
                            Console.WriteLine("Comentarios: {0}",
                                              p[numero].comentarios);

                            Console.WriteLine("Desea borrar la ficha?" +
                                              " (s/n) ");
                            aux = Console.ReadLine().ToLower();
                            if (aux == "s")
                            {
                                for (int i = numero; 
                                    i < countProgramadores; i++)
                                {
                                    p[i] = p[i + 1];
                                }
                                countProgramadores--;
                            }

                        }
                    }
                    break;

                case "6"://Estadisticas
                    Console.WriteLine("Hay {0} programadores",
                                      countProgramadores);
                    int posJoven = 0, posVeterano = 0, cantPlantilla = 0;
                    double cantidadHabilidades = 0;
                    string[] hab;
                    for (int i = 0; i < countProgramadores; i++)
                    {
                        if (p[i].anyo != 0)
                            posJoven = posVeterano = i;
                        if (p[i].plantilla)
                            cantPlantilla++;
                        hab = p[i].habilidades.Split(',');
                        if(hab[0] != "")
                            cantidadHabilidades += hab.Length;
                    }

                    for (int i = 0; i < countProgramadores; i++)
                    {
                        if (p[i].anyo <= p[posVeterano].anyo)
                            posVeterano = i;
                        if (p[i].anyo >= p[posJoven].anyo)
                            posJoven = i;
                    }
                    double porcentajePlantilla;
                    try
                    {
                        porcentajePlantilla =
                            (countProgramadores / cantPlantilla);
                        porcentajePlantilla = 100 / porcentajePlantilla;
                    } catch (Exception e) {
                        porcentajePlantilla = 0;
                    }
                    Console.WriteLine("Porcentaje en plantilla: {0}%",
                                      porcentajePlantilla.ToString("0.0"));
                    if (p[posJoven].anyo != 0)
                        Console.WriteLine("Año del mas joven: " +
                                          p[posJoven].anyo);
                    else
                        Console.WriteLine("No hay datos del mas joven");

                    if (p[posVeterano].anyo != 0)
                        Console.WriteLine("Año del mas veterano: " +
                                          p[posVeterano].anyo);
                    else
                        Console.WriteLine("No hay datos del mas veterano");

                    Console.WriteLine("Media de habilidades: " +
                                      cantidadHabilidades/countProgramadores);

                    break;
                case "7":
                    Console.WriteLine("Introduzca habilidades a contar," +
                                      " separado por espacios");

                    string[] habilidades =
                        Console.ReadLine().ToLower().Split();
                    int[] canthabilidad = new int[habilidades.Length];

                    for (int i = 0; i < habilidades.Length; i++)
                    {
                        for (int j = 0; j < countProgramadores; j++)
                        {
                            if (p[j].habilidades.Contains(habilidades[i]))
                                canthabilidad[i]++;
                        }
                    }
                    for (int i = 0; i < habilidades.Length; i++)
                    {
                        Console.WriteLine(habilidades[i]
                                          + ": " + canthabilidad[i]);
                    }
                    break;
            }
        } while (opcion.ToLower() != "t");
    }
}
