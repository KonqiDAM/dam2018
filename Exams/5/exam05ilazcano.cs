//Ivan Lazcano Sindin
using System;
public class Exam05ilazcano
{
    public static bool EsHappyNumber(long n)
    {
        string sn = n.ToString();
        long n2 = 0;
        bool found = false;
        while (!found)
        {
            n2 = 0;
            foreach (char l in sn)
            {
                n2 += Convert.ToInt64(l - '0') * Convert.ToInt64(l - '0');
            }
            sn = n2.ToString();
            if (n2 == 1 || n2 == 4)
                found = true;
        }
        return n2 == 1;
    }


    public static void ContarAlfaEsp(ref int alfa,
                                     ref int espacios,
                                     string texto)
    {
        alfa = 0;
        espacios = 0;
        foreach (char l in texto)
        {
            if (l == ' ')
                espacios++;
            else if (l >= 'a' && l <= 'z' || l >= 'A' && l <= 'Z')
                alfa++;
        }
    }

    public static void DibujarRombo(int ancho, char c)
    {
        int espacios = (ancho - 1) / 2;
        int caracters = ancho % 2 == 0 ? 2 : 1;
        while (caracters != ancho)
        {
            for (int i = 0; i < espacios; i++)
            {
                Console.Write(' ');
            }
            for (int i = 0; i < caracters; i++)
            {
                Console.Write(c);
            }
            Console.WriteLine();
            caracters += 2;
            espacios--;
        }
            


        while (caracters > 0)
        {
            for (int i = 0; i < espacios; i++)
            {
                Console.Write(' ');
            }
            for (int i = 0; i < caracters; i++)
            {
                Console.Write(c);
            }
            Console.WriteLine();
            caracters -= 2;
            espacios++;
        }
    }

    public static bool BinSearch(int[] data, int search, int ini, int fin)
    {
        int mid;

        do
        {
            mid = (ini + fin) / 2;
            if (data[mid] == search)
                return true;
            if (ini == mid && data[ini] != search)
                return false;

            if (data[mid] < search)
                ini = mid;
            else
                fin = mid;

        } while (ini != fin);

        return true;
    }

    public static bool BinSearchR(int[] data, int search, int ini, int fin)
    {
        int mid = (ini + fin) / 2;
        if (ini == mid && data[ini] != search)
            return false;

        if (data[mid] == search || data[fin] == search
            || data[ini] == search)
            return true;
        else if (data[mid] > search)
            BinSearchR(data, search, ini, mid);
        else
            BinSearchR(data, search, mid, fin);

        return false;
    }

    public static int Main(string[] args)
    {
        if (args.Length == 0)
        {
            Console.WriteLine("Uso: happy / contar / rombo / binsearch");
            return 2;
        }
        else if (args.Length == 1)
        {
            Console.WriteLine("Faltan detalles");
            return 1;
        }
        else
            switch (args[0])
            {
                case "happy":
                    Console.WriteLine(EsHappyNumber(Convert.ToInt64(args[1]))
                                      ? "Es un numero feliz"
                                      : "No es un numero feliz");
                    break;

                case "contar":

                    string frase = "";
                    for (int i = 1; i < args.Length; i++)
                    {
                        frase += args[i];
                    }
                    int alfa = 0, espacios = 0;
                    ContarAlfaEsp(ref alfa, ref espacios, frase);
                    Console.WriteLine("Alfabeticos: {0}, Espacios: {1}",
                                      alfa, args.Length - 2);
                    break;

                case "rombo":
                    if (args.Length != 3)
                    {
                        Console.WriteLine("Faltan detalles");
                        return 1;
                    }
                    else
                        DibujarRombo(Convert.ToInt32(args[1]), args[2][0]);
                    break;

                case "binsearch":
                    int[] data = { 10, 20, 30, 40, 50, 60, 70 };
                    Console.WriteLine(BinSearchR(data, Convert.ToInt32(args[1]),
                                              0, data.Length - 1)
                                  ? "Esta en el array"
                                  : "No esta en el array"); ;
                    break;

                default:
                    Console.WriteLine("Uso: happy / contar " +
                                      "/ rombo / binsearch");
                    return 2;
                    break;
            }
        return 0;
    }
}
