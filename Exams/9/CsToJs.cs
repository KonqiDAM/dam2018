//Ivan Lazcano Sindin
using System.IO;
using System;

class CsharpToJS
{
    public static void Parser(StreamWriter writeFile, string line)
    {
        int aux;


        if (line.StartsWith("    {"))
            line = line.Remove(0,"    {".Length);

        if(line.Contains("        "))
            line = line.Replace("        ", "");
        

           
        if (line.Trim().StartsWith("Console.WriteLine"))
        {
            aux = line.IndexOf("Console.WriteLine");
            line = line.Remove(aux, "Console.WriteLine".Length );
            line = line.Insert(aux, "console.log");

        }

        if (line.Contains("=="))
            line = line.Replace("==", "===");
        if (line.Contains("!="))
            line = line.Replace("!=", "!==");
        if (line.Contains(".ToUpper("))
            line = line.Replace(".ToUpper(", ".toUpperCase(");
        if (line.Contains(".Trim("))
            line = line.Replace(".Trim(", ".trim(");
        if (line.Contains(".Replace("))
            line = line.Replace(".Replace(", ".replace(");

        string[] types = { "int", "string", "long", "byte", "char" };

        foreach (string s in types)
        {
            if (line.Trim().StartsWith(s))
            {
                aux = line.IndexOf(s);
                line = line.Remove(aux, s.Length);
                line = line.Insert(aux, "let");
            }
        }

        if (line.Contains("using System"))
            return;
        if (line.StartsWith("class "))
            return;
        if (line.Contains("static void Main"))
        {

            return;

        }

        if (line.Contains(".split("))
        {
            line = line.Replace("(\'", "(\"").Replace("\')", "\")");
        }

        if (line.StartsWith("{"))
            return;


        if(line.Trim().Contains("foreach"))
        {
            line = line.Replace("foreach", "for").Replace(" in ", " of ");

            foreach (string s in types)
            {
                if (line.Contains(s))
                {
                    aux = line.IndexOf(s);
                    line = line.Remove(aux, s.Length);
                    line = line.Insert(aux, "let");
                }
            }

        }

        writeFile.WriteLine(line);


    }

    public static void Main(string[] args)
    {
        string name;
        if (args.Length >= 1)
            name = args[0];
        else
        {
            Console.WriteLine("Name?");
            name = Console.ReadLine();
        }

        if (!File.Exists(name))
        {
            Console.WriteLine("File does not exist!");
        }
        else
        {
            try
            {
                StreamReader readFile = new StreamReader(name);
                StreamWriter writeFile =
                    new StreamWriter(name.Replace(".cs", ".js"));

                string line = readFile.ReadLine();

                while (line != null)
                {
                    Parser(writeFile, line);
                    line = readFile.ReadLine();
                }

                writeFile.Close();
                readFile.Close();

            }
            catch (PathTooLongException)
            {
                Console.WriteLine("Path to long");
            }
            catch (IOException)
            {
                Console.WriteLine("IOerror");
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }
    }
}

