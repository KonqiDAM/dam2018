//Ivan Lazcano Sindin
using System.IO;
using System;

class PGMConversor
{
    public static void Main(string[] args)
    {
        string fileName;
        if (args.Length >= 1)
            fileName = args[0];
        else
        {
            Console.WriteLine("Name?");
            fileName = Console.ReadLine();
        }
        
        if (!File.Exists(fileName))
        {
            Console.WriteLine("File does not exist!");
        }
        else
        {
            try
            {
                StreamWriter writeFile =
                    new StreamWriter(fileName.Substring(
                        0, fileName.Length - 4) + "2.pgm");
                StreamReader readFile = new StreamReader(fileName);
                string line = readFile.ReadLine();

                if (line != null && line != "P2")
                {
                    Console.WriteLine("its not P2!");
                    writeFile.Close();
                    readFile.Close();
                    return;
                }
                else
                    writeFile.WriteLine("P5");

                line = readFile.ReadLine();
                if (line != null && line.Contains("#"))
                {
                    writeFile.WriteLine(line);
                    line = readFile.ReadLine();
                }

                if (line != null)
                    writeFile.WriteLine(line);

                line = readFile.ReadLine();
                if (line != null)
                    writeFile.WriteLine(line);

                writeFile.Close();

                FileStream writeFileBinary 
                = File.OpenWrite(fileName.Substring(
                    0, fileName.Length - 4) + "2.pgm");

                writeFileBinary.Seek(0, SeekOrigin.End);

                line = readFile.ReadLine();
                while (line != null)
                {
                    foreach (string s in line.Split())
                    {
                        try
                        {
                            writeFileBinary.WriteByte(Convert.ToByte(s));
                        } catch (Exception)
                        {
                            ;
                        }
                    }
                    line = readFile.ReadLine();
                }
                writeFileBinary.Close();
                readFile.Close();
            }
            catch (PathTooLongException)
            {
                Console.WriteLine("Path to long");
            }
            catch (IOException e)
            {
                Console.WriteLine("IOerror: " + e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }
    }
}

