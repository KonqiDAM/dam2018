//Ivan Lazcano Sindin
using System.Net;
using System.IO;

class FetchAceptaElReto
{
    public static void Main(string[] args)
    {
        for (int i = 100; i < 110; i++)
        {
            WebClient client = new WebClient();
            Stream conexion =
                client.OpenRead("https://www.aceptaelreto.c" +
                                "om/problem/statement.php?id="+i);
            StreamReader lector = new StreamReader(conexion);
            string linea;
            StreamWriter fileOut = new StreamWriter("problema_" + i + ".html");
            while ((linea = lector.ReadLine()) != null)
                fileOut.WriteLine(linea);
            fileOut.Close();
            lector.Close();
            conexion.Close();
        }
    }
}
