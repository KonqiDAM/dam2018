//Ivan Lazcano Sindin
using System;
using System.IO;
class DateTest
{
    public static void Main(string[] args)
    {
        string[] month = { "Enero", "Febrero", "Marzo", "Abril", "Mayo",
            "Junio", "Jullio", "Agosto", "Septiembre",
            "Octubre", "Noviembre", "Diciembre" };
        DateTime date = DateTime.Now;
        try
        {
            StreamWriter outFile = new StreamWriter("task.txt");
            while (date < new DateTime(2020, 1, 1))
            {
                outFile.WriteLine(date.Day + "-" + month[date.Month - 1] + "-" + date.Year.ToString().Substring(2, 2));
                date = date.AddDays(7);
            }
            outFile.Close();
        } catch (Exception)
        {
            Console.WriteLine("Error");
        }

    }
}
