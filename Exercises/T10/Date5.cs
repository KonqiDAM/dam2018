//Ivan Lazcano Sindin
using System;
using System.Threading;

class DateTest
{
    public static void Main(string[] args)
    {
        Console.ForegroundColor = ConsoleColor.Cyan;
        Console.BackgroundColor = ConsoleColor.Blue;
        Console.Clear();
        while(true)
        {
            Console.SetCursorPosition(72,0);
            Console.WriteLine(DateTime.Now.Hour.ToString("00")
                              +":"+DateTime.Now.Minute.ToString("00")
                              +":"+DateTime.Now.Second.ToString("00"));
            Thread.Sleep(1000);
        }
    }
}
