//Ivan Lazcano Sindin
using System;

class Calendar
{
    public static void Main(string[] args)
    {
        DateTime fecha = 
            new DateTime(Convert.ToInt32(args[1]), Convert.ToInt32(args[0]), 1);
        
        Console.WriteLine("\t" + fecha.Month + " " + fecha.Year);
        Console.WriteLine("lu ma mi ju vi sa do");
        for (int i = 0; i < (int)fecha.DayOfWeek - 1; i++)
        {
            Console.Write("   ");
        }
        do
        {
            Console.Write(fecha.Day.ToString("00" + " "));
            if ((int)(fecha.DayOfWeek) == 0)
                Console.WriteLine();
            fecha = fecha.AddDays(1);
        } while (fecha.Month == Convert.ToInt32(args[0]));
    }
}
