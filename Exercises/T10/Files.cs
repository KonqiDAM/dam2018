//Ivan Lazcano Sindin
using System;
using System.IO;

class FilesInActualDir
{
    public static void Main(string[] args)
    {
        DirectoryInfo dir = new DirectoryInfo(".");
        FileInfo[] ficheros = dir.GetFiles();

        foreach(FileInfo s in ficheros)
        {
            if(s.Name.EndsWith(".cs"))
                Console.WriteLine("Name: " 
                                  + s +", Size: " + s.Length/1024.0 +"KB");
        }
    }
}
