//Ivan Lazcano Sindin
using System.Net;
using System.IO;
using System;


class LinqTest
{
    public static void Main(string[] args)
    {
        Console.WriteLine("Enter 10 integers data:");
        const int N = 10;
        int[] data = new int[N];
        for (int i = 0; i < N ; i++)
        {
            Console.Write("Enter " + (i+1));
            data[i] = Convert.ToInt32(Console.ReadLine());
        }

        var numQuery =
            from num in data
            where num > 0
            select num;

        foreach (int num in numQuery)
        {
            Console.Write("{0,1} ", num);
        }

    }
}
