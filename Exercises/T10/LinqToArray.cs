//Ivan Lazcano
using System;
using System.Linq;

public class LinqTest
{
    public static void Main()
    {
        string s1 = "1 2 3 4 5 6 7 8 9 10 11 12";
        int[] ia = s1.Split().Select(n => Convert.ToInt32(n)).ToArray();
        foreach(int i in ia)
            Console.WriteLine(i);
    }
}
