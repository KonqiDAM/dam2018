//Ivan Lazcano Sindin
using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;


class ReegeexTester
{
    public static bool IsVarCorrecVarName(String s)
    {
        return new Regex(@"\A[A-Za-z][A-Za-z0-9]*\Z").IsMatch(s);
    }

    public static void Main(string[] args)
    {
        Dictionary<string, bool> test = new Dictionary<string, bool>();
        test.Add("asdasd", true);
        test.Add("ASDASD", true);
        test.Add("ASD2", true);
        test.Add("2asd", false);
        test.Add("asd2asd", true);
        test.Add("d33333", true);
        test.Add("lll4", true);
        test.Add("2331", false);
        test.Add("asd_asd", false);

        foreach(string s in test.Keys )
        {
            Console.WriteLine( s + "->" +IsVarCorrecVarName(s) +"->"+test[s] );
        }

    }
}
