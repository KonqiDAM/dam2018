//Ivan Lazcano Sindin
using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;


class ReegeexTester
{
    public static bool IsVarCorrecVarName(String s)
    {
        return new Regex(@"\A[a-z][a-z]*@[a-z][a-z]*[.][a-z]{2,4}\Z").IsMatch(s);
    }

    public static void Main(string[] args)
    {
        Dictionary<string, bool> test = new Dictionary<string, bool>();
        test.Add("asd@asd.com", true);
        test.Add("asd@asdcom", false);
        test.Add("asdasd.com", false);
        test.Add("a2sd@as.d.com", false);
        test.Add("asd@as.com2", false);
        test.Add("asd@as.d.comdddd", false);
        test.Add("@as.d.comdddd", false);
        test.Add("2asd@as.d.comdddd", false);
        test.Add("asd@.comdddd", false);
        test.Add("a@a.ddsa", true);


        foreach(string s in test.Keys )
        {
            if(IsVarCorrecVarName(s) != test[s])
            {
                Console.WriteLine(s + "->" + IsVarCorrecVarName(s) + "->" + test[s]);
            }
            else
            {
                Console.WriteLine(s + "->ok");
            }
        }

    }
}
