//Ivan Lazcano Sindin
using System.Net;
using System.IO;

class RubyDownloader
{
    public static void Main(string[] args)
    {
    for (int i = 0; i < 4; i++)
        {
            WebClient client = new WebClient();
            client.DownloadFile(
                "https://learnrubythehardway.org/book/ex" 
                + i + ".html", "book" + i + ".html");
            System.Console.Write(i + " ");
        }
    }
}
