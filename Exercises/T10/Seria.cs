//Ivan Lazcano Sindin

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable]
class Person
{
    private string name;
    private int age;

    public Person(string name, int age)
    {
        this.name = name;
        this.age = age;
    }

    public int GetAge() { return age; }
    public string GetName() { return name; }

    public void SetAge(int age) { this.age = age; }
    public void Setname(string name) { this.name = name; }


    public static void Save(Person p)
    {
        IFormatter form = new BinaryFormatter();
        Stream fichero = new FileStream("save.txt", FileMode.Create, FileAccess.Write, FileShare.None);
        form.Serialize(fichero, p);
        fichero.Close();
    }

    public static Person Load()
    {
        Person p = new Person("", 0);
        IFormatter form = new BinaryFormatter();
        Stream fichero = new FileStream("save.txt", FileMode.Open, FileAccess.Read, FileShare.Read);
        p = (Person)form.Deserialize(fichero);
        fichero.Close();
        return p;
    }

    public override string ToString()
    {
        return string.Format("Age: " + age + ", Name: " +name);
    }
}

class MainProg
{
    public static void Main()
    {
        Person p = new Person("Name here", 22);
        Person.Save(p);
        Console.WriteLine(p);
        p.SetAge(444);
        Console.WriteLine(p);
        p = Person.Load();
        Console.WriteLine(p);
    }
}
