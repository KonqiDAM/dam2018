//Ivan Lazcano Sindin
using System.Net;
using System.IO;
using System;


class Info
{
    public static void Main(string[] args)
    {


        Console.WriteLine("Version: " + Environment.OSVersion);
        Console.WriteLine("User name: " + Environment.UserName);
        Console.WriteLine("C# version: " + Environment.Version);
        Console.WriteLine("Documentos: " + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));

        string[] disk = Environment.GetLogicalDrives();
        foreach (string s in disk)
        {
            DriveInfo f = new DriveInfo(s);
            if (f.IsReady)
            {
                Console.Write("Drive: " + s + " -> ");
                Console.WriteLine("Total/Free: "
                                  + f.TotalSize / 1024 / 1024 + "/"
                                  + f.TotalFreeSpace / 1024 / 1024);
            }
        }

    }
}
