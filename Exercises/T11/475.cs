//Ivan Lazcano
using System;
using System.Data.SQLite;
using System.IO;

class MainClass
{
    public static void Main(string[] args)
    {
        SQLiteCommand cmd;
        SQLiteConnection conexion;
        if (!File.Exists("series.sqlite"))
        {
            conexion =
              new SQLiteConnection
                ("Data Source=series.sqlite;Version=3;New=True;Compress=True;");
            conexion.Open();

            // Creamos la tabla
            string creacion = "create table series "+
                "("+
                "    id int primary key,"+
                "    titulo varchar(50),"+
                "    genero varchar(25),"+
                "    anyo int"+
                "); ";
            cmd = new SQLiteCommand(creacion, conexion);
            cmd.ExecuteNonQuery();
        }
        else
        {
            conexion =
              new SQLiteConnection
                ("Data Source=series.sqlite;Version=3;New=False;Compress=True;");
            conexion.Open();
        }

        string opcion;
        bool continuar = true;
        while(continuar)
        {
            Console.WriteLine("1. Agregar");
            Console.WriteLine("2. Ver todo");
            Console.WriteLine("3. Salir");
            opcion = Console.ReadLine();
            switch(opcion)
            {
                case "1":
                    Console.WriteLine("Titulo?");
                    string nombre = Console.ReadLine();
                    Console.WriteLine("Año?");
                    int anyo = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Genero?");
                    string genero = Console.ReadLine();
                    Console.WriteLine("ID?");
                    int id = Convert.ToInt32(Console.ReadLine());
                    string insercion = "insert into series values " +
                        "("+id+", "+nombre+", "+genero+", "+anyo+");";
                    cmd = new SQLiteCommand(insercion, conexion);
                    break;
                case "2":
                    string selectTodo = "select id, titulo, genero, anyo from series;";
                    cmd = new SQLiteCommand(selectTodo, conexion);
                    SQLiteDataReader datos = cmd.ExecuteReader();
                    while (datos.Read())
                    {
                        Console.WriteLine(datos[1] + " " + datos[3]);
                    }
                    break;
                default:
                    continuar = false;
                    break;
            }

        }
        conexion.Close();
    }
}

