//Ivan Lazcano Sindin
using System;
using System.IO;

class Ej
{
    public static void Main(string[] args)
    //Ivan Lazcano Sindin
using System;
using System.IO;

class Ej
{
    public static void Main(string[] args)
    {
        if (!File.Exists(args[0]))
            Console.WriteLine("Origin does not exist");
        else
        {
            try
            {
                BinaryReader readFile 
                = new BinaryReader(File.Open(args[0], FileMode.Open));
                if(!(readFile.ReadChar() == 'B' 
                     && readFile.ReadChar() == 'M'))
                {
                    Console.WriteLine("Not bmp");
                }
                else
                {
                    readFile.BaseStream.Seek(18,0);
                    Console.WriteLine("Width: " + readFile.ReadInt32());
                    Console.WriteLine("Hight: " +readFile.ReadInt32());
                    readFile.BaseStream.Seek(30, 0);
                    Console.WriteLine(
                        readFile.ReadInt32() == 0 
                        ? "Not Compressed" : "Compressed");
                }

                readFile.Close();

            }
            catch(PathTooLongException)
            {
                Console.WriteLine("Name to long");
            }
            catch (IOException)
            {
                Console.WriteLine("IO error");
            }
            catch (Exception)
            {
                Console.WriteLine("Error on file!");
            }
        }
    }
}
{
        if (!File.Exists(args[0]))
            Console.WriteLine("Origin does not exist");
        else
        {
            try
            {
                BinaryReader readFile 
                = new BinaryReader(File.Open(args[0], FileMode.Open));
                if(!(readFile.ReadChar() == 'B' 
                     && readFile.ReadChar() == 'M'))
                {
                    Console.WriteLine("Not bmp");
                }
                else
                {
                    readFile.BaseStream.Seek(18,0);
                    Console.WriteLine("Ancho: " + readFile.ReadInt32());
                    Console.WriteLine("Alto: " +readFile.ReadInt32());
                    readFile.BaseStream.Seek(30, 0);
                    Console.WriteLine(
                        readFile.ReadInt32() == 0 
                        ? "No comprimido" : "Comprimido");
                }

            }
            catch(PathTooLongException)
            {
                Console.WriteLine("Name to long");
            }
            catch (IOException)
            {
                Console.WriteLine("IO error");
            }
            catch (Exception)
            {
                Console.WriteLine("Error on file!");
            }
        }
    }
}
