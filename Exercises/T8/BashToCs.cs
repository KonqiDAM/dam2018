//Ivan Lazcano Sindin
using System;
using System.IO;
using System.Diagnostics;

class Ej
{
    public static void Main(string[] args)
    {

        if (!File.Exists(args[0]))
            Console.WriteLine("Origin does not exist");
        else
        {
            try
            {
                StreamWriter writeFile = new StreamWriter(args[0] + ".cs");

                writeFile.Write("using System;\nusing System.IO;" +
                                "using System.Diagnostics;\n"
                                + "class Ej\n{"
                                + "\tpublic static void Main(string[] args)"
                                + "\n\t{");

                StreamReader readFile = new StreamReader(args[0]);
                writeFile.WriteLine("Process proc;");
                string line = readFile.ReadLine();
                string first;
                while (line != null)
                {
                    first = line.ToLower().Split()[0];

                    if (first == "cls")
                    {
                        writeFile.WriteLine("\tConsole.Clear();");
                    }
                    else if (first == "echo")
                    {
                        writeFile.WriteLine("\techo " +line.Substring
                                            (line.IndexOf(" ")+1));
                    }
                    else if (first == "cd")
                    {
                        writeFile.WriteLine("\tDirectory." +
                                "SetCurrentDirectory("
                                + line.Substring(line.IndexOf(" ")+1) + ");");
                    }
                    else
                        writeFile.WriteLine("\tProcess.Start("
                                    + line + ");proc.WaitForExit();");

                    line = readFile.ReadLine();
                }

                writeFile.WriteLine("\t}\n}");

                writeFile.Close();
                readFile.Close();

            }
            catch (PathTooLongException)
            {
                Console.WriteLine("Name to long");
            }
            catch (IOException)
            {
                Console.WriteLine("IO error");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
