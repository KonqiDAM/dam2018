//Ivan Lazcano Sindin
using System;
using System.IO;

class Ej
{
    public static void Main(string[] args)
    {
        if(!File.Exists(args[0]))
            Console.WriteLine("Origin does not exist");
        else
            try
            {
                FileStream readFile = File.OpenRead(args[0]);
                byte[] data = new byte[readFile.Length];

                readFile.Read(data, 0, data.Length);
                readFile.Close();

                if(File.Exists(args[1]))
                    Console.WriteLine("File exists...");
                else
                {
                    FileStream writeFile = File.Create(args[1]);
                    writeFile.Write(data, 0, data.Length);
                    writeFile.Close();
                }
                
            }
            catch (Exception)
            {
                Console.WriteLine("Error on file!");
            }



    }
}
