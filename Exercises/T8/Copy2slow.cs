//Ivan Lazcano Sindin
using System;
using System.IO;

class Ej
{
    public static void Main(string[] args)
    {
        if (!File.Exists(args[0]))
            Console.WriteLine("Origin does not exist");
        else
            try
            {
                FileStream readFile = File.OpenRead(args[0]);

                if (File.Exists(args[1]))
                    Console.WriteLine("File exists...");
                else
                {
                    FileStream writeFile = File.Create(args[1]);
                    for (int i = 0; i < readFile.Length; i++)
                    {
                        writeFile.WriteByte((byte)readFile.ReadByte());

                    }
                    writeFile.Close();
                    readFile.Close();
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Error on file!");
            }
    }
}
