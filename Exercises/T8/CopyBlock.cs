//Ivan Lazcano Sindin
using System;
using System.IO;

class Ej
{
    public static void Main(string[] args)
    {
        if (!File.Exists(args[0]))
            Console.WriteLine("Origin does not exist");
        else
        {
            try
            {
                int count = Convert.ToInt32(args[2]);
                FileStream readFile = File.OpenRead(args[0]);
                byte[] data = new byte[count];
                FileStream writeFile = File.OpenWrite(args[1]);

                long length = readFile.Length;

                int cantidadLeida = 0;

                cantidadLeida = readFile.Read(data, 0, count);
                writeFile.Write(data, 0, cantidadLeida);

                while(cantidadLeida == count && cantidadLeida != length)
                {
                    cantidadLeida = readFile.Read(data, 0, count);
                    writeFile.Write(data, 0, cantidadLeida);
                }

                writeFile.Close();
                readFile.Close();

            }
            catch(PathTooLongException)
            {
                Console.WriteLine("Name to long");
            }
            catch (IOException)
            {
                Console.WriteLine("IO error");
            }
            catch (Exception)
            {
                Console.WriteLine("Error on file!");
            }
        }
    }
}
