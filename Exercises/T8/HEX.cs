//Ivan Lazcano Sindin
using System;
using System.IO;
using System.Diagnostics;

class Ej
{
    public static void Main(string[] args)
    {

        if (!File.Exists(args[0]))
            Console.WriteLine("Origin does not exist");
        else
        {
            try
            {
                FileStream readFile = File.OpenRead(args[0]);

                byte[] data = new byte[16];
                long countRead = 0;
                do
                {
                    countRead = readFile.Read(data, 0, 16);
                    for (int i = 0; i < 16; i++)
                    {
                        Console.Write((data[i]).ToString("x2") + " ");
                    }
                    Console.Write("  ");
                    for (int i = 0; i < 16; i++)
                    {
                        Console.Write(data[i] >= 32 ? (char)data[i]: '.');
                    }

                    Console.WriteLine();
                } while (countRead == 16);


                    readFile.Close();

            }
            catch (PathTooLongException)
            {
                Console.WriteLine("Name to long");
            }
            catch (IOException)
            {
                Console.WriteLine("IO error");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
