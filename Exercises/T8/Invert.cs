//Ivan Lazcano Sindin
using System;
using System.IO;

class Ej
{
    public static void Main(string[] args)
    {
        if (!File.Exists(args[0]))
            Console.WriteLine("Origin does not exist");
        else
        {
            try
            {
                byte aux;
                FileStream readFile = File.OpenRead(args[0]);

                if (File.Exists(args[1]))
                    Console.WriteLine("File exists...");
                else
                {
                    FileStream writeFile = File.Create(args[1]);
                    for (int i = (int)readFile.Length-1; i >= 0 ; i--)
                    {
                        readFile.Seek(i,0);
                        writeFile.WriteByte((byte)readFile.ReadByte());

                    }
                    writeFile.Close();
                    readFile.Close();
                }
            }
            catch(PathTooLongException)
            {
                Console.WriteLine("Name to long");
            }
            catch (IOException)
            {
                Console.WriteLine("IO error");
            }
            catch (Exception)
            {
                Console.WriteLine("Error on file!");
            }
        }
    }
}
