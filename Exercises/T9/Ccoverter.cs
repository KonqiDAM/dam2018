//Ivan Lazcano Sindin
using System;
using System.IO;

class Ej
{
    public static void Main(string[] args)
    {
        string name;
        if (args.Length < 1)
        {
            Console.WriteLine("Name?");
            name = Console.ReadLine();
        }
        else
            name = args[0];
            
        if (!File.Exists(name))
            Console.WriteLine("Origin does not exist");
        else
        {
            try
            {
                StreamReader fileRead = new StreamReader(name);
                StreamWriter outFile = 
                    new StreamWriter(name.Replace(".cs", ".c"));

                string line = fileRead.ReadLine();
                int taps = 0;
                while(line != null)
                {
                    if (line.Contains("Using"))
                    {
                        outFile.WriteLine("#include <stdio.h>");
                    }
                    else if (line == "")
                        outFile.WriteLine("");
                    else if (line.Contains("class"))
                    {
                        line = fileRead.ReadLine();
                    }
                    else if (line.Contains("public static void Main()"))
                    {
                        outFile.WriteLine("int main()\n{");
                        taps++;
                        line = fileRead.ReadLine();
                    }
                    else if(line.Contains("Console.ReadLine()"))
                    {
                        outFile.WriteLine("int "
                              +line.Split()[1]+
                              "; scanf(\" % d\",&"
                              +line.Split()[1]+");");
                    }
                    else if(line.Contains("for (int "))
                    {
                        outFile.WriteLine("int "
                                          + line.Split()[2]
                                          + "; "
                                          + line.Replace("int ", ""));
                    }
                    else if(line.Contains("Console.Write"))
                    {
                        bool wl = line.Contains("Console.WriteLine");
                        if(wl)
                            line = line.Replace("Console.WriteLine(", "printf(");
                        else
                            line = line.Replace("Console.Write", "printf");

                        for (int i = 0; i < 20; i++)
                        {
                            if (line.Contains('{' + i + "}"))
                                line = line.Replace('{' + i + "}", "%d");
                        }

                        outFile.WriteLine(line + (wl ? "\n" : ""));
                    }
                    else if(line.Contains("//"))
                    {
                        line.Replace("//","/*");
                        line = line + "*/";
                        outFile.WriteLine(line);
                    }
                    else if(line.Contains("{"))
                    {
                        taps++;
                        outFile.WriteLine("{");
                    }
                    else if(line.Contains("}"))
                    {
                        taps--;
                        outFile.WriteLine("}");
                    }


                    line = fileRead.ReadLine();
                }
                fileRead.Close();
                outFile.Close();
            }
            catch (PathTooLongException)
            {
                Console.WriteLine("Name to long");
            }
            catch (IOException)
            {
                Console.WriteLine("IO error");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
