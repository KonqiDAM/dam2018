//Ivan Lazcano Sindin
//GNU GPLv3
//https://www.gnu.org/licenses/gpl-3.0.en.html

using System.IO;
using System;
class GcodeGenerator
{
    static void Main(string[] args)
    {
        string fileName = "";
        if(args.Length > 0)
        {
            if (File.Exists(args[0]))
                fileName = args[0];
        }

        while (!File.Exists(fileName))
        {
            Console.WriteLine("Input array?");
            fileName = Console.ReadLine();
        }



        float xOffset = 104;
        float yOffset = 100;
        float zOffset = 1.75f;
        float zLift = 2f;
        float resolution = 1f;
        bool isLifted = false;
        string customText = "Hello World";
        string headers = "G28 ; Home all axes\r\nG21; set units to millimeters\r\nG90; use absolute coordinates";
        string endFile = "G1 Z" + (25+zOffset) + " ;\r\nM84 ; PARAR MOTORES\r\nM220 S100 ; Reset Speed factor override percentage to default (100%)";
        float xPos = 0, yPos = 0;


        try {
            StreamWriter outFile = new StreamWriter(fileName.Split('.')[0]+".gcode");
            outFile.WriteLine(headers);
            outFile.WriteLine("M117 " + customText);
            outFile.WriteLine("G1 Z" + (zOffset+zLift));
            isLifted = true;

            StreamReader inputFile = new StreamReader(fileName);
            string line = inputFile.ReadLine();
            yPos = yOffset;
            while(line != null)
            {
                xPos = xOffset;
                yPos += resolution;
                for (int i = 0; i < line.Length; i++)
                {
                    if(line[i] == ' ')
                    {
                        if(!isLifted)
                        {
                            outFile.WriteLine("G1 Z"+(zOffset+zLift));
                            isLifted = true;
                        }
                    }
                    else if(line[i] == '#')
                    {
                        outFile.WriteLine("G1 X"+(xPos)+" Y"+(yPos));
                        if (isLifted)
                        {
                            outFile.WriteLine("G1 Z" + (zOffset));
                            isLifted = false;
                        }
                    }
                    xPos += resolution;
                }
                isLifted = true;
                outFile.WriteLine("G1 Z" + (zOffset + zLift));
                line = inputFile.ReadLine();
            }

            outFile.WriteLine(endFile);
            outFile.Close();
            inputFile.Close();

        } catch (PathTooLongException)
        {
            Console.WriteLine("path");
        } catch (IOException)
        {
            Console.WriteLine("IOerror");
        } catch (Exception)
        {
            Console.WriteLine("Error");
        }


    }
}

