//Ivan Lazcano Sindin
using System;
using System.IO;

class Ej
{

    public static void Draw(short color)
    {
        char c;
        if (color >= 200)
            c = ' ';
        else if (color >= 150)
            c = '.';
        else if (color >= 100)
            c = '-';
        else if (color >= 50)
            c = '=';
        else
            c = '#';

        Console.Write(c);
    }

    public static void Main(string[] args)
    {
        if (args.Length >= 1 && !File.Exists(args[0]))
            Console.WriteLine("Origin does not exist");
        else
        {
            try
            {
                FileStream img = File.OpenRead(args[0]);
                byte[] imgData = new byte[img.Length];
                int countRead = img.Read(imgData, 0, (int)img.Length);
                if(countRead != img.Length)
                {
                    Console.WriteLine("error reading file");
                    return;
                }
                img.Close();

                if(!(imgData[0] == 10))
                {
                    Console.WriteLine("File tipe error");

                }
                else if(imgData[2] != 1)
                {
                    Console.WriteLine("Not compressed!");
                }
                else if(imgData[3] != 8)
                    Console.WriteLine("Error on color!");
                else
                {

                    short xMin = (short)(imgData[4] + imgData[5] * 256);
                    short yMin = (short)(imgData[6] + imgData[7] * 256);
                    short xMax = (short)(imgData[8] + imgData[9] * 256);
                    short yMax = (short)(imgData[10] + imgData[11] * 256);


                    Console.WriteLine("Bits per pixel: " + imgData[3]);
                    Console.WriteLine("Width: " + (xMax - xMin + 1));
                    Console.WriteLine("Height: " + (yMax - yMin + 1));
                    Console.WriteLine("Bytes per line: " + imgData[66]);

                    if(imgData[66] != xMax-xMin+1)
                        Console.WriteLine("Error on width");
                    else
                    {
                        int position = 128;
                        for (int i = 0; i < (yMax-yMin+1); i++)
                        {
                            for (int j = 0; j < imgData[66]; j++)
                            {
                                if(imgData[position] < 192)
                                {
                                    Draw(imgData[position]); 
                                }
                                else
                                {
                                    //position++;

                                    for (int k = 0; k < imgData[position]-192; k++)
                                    {
                                        position++;
                                        Draw(imgData[position]);
                                    }
                                }
                                position++;
                            }
                            Console.WriteLine();
                        }
                    }
                }
            }
            catch (PathTooLongException)
            {
                Console.WriteLine("Name to long");
            }
            catch (IOException)
            {
                Console.WriteLine("IO error");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
