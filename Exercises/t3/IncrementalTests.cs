//Ivan Lazcano
using System;
class MainClass
{
    public static void Main()
    {
        int a = Convert.ToInt32(Console.ReadLine());
        int b = Convert.ToInt32(Console.ReadLine());
        int c = Convert.ToInt32(Console.ReadLine());
        
        ++a;
        b /= 3;
        c -= 5;
        
        Console.WriteLine("Primer numero incrementado: {0}", a);
        Console.WriteLine("Segundo numero dividido entre 3: {0}", b);
        Console.WriteLine("Tercer numero disminuido en 5: {0}", c);
        
        int d1 = 8, e1 = 8;
        int d2 = --d1;
        int e2 = e1--;
        Console.WriteLine("d1: {0}, d2: {1}, e1: {2}, e2: {3}, ", 
                d1, d2, e1, e2);
    }
}
