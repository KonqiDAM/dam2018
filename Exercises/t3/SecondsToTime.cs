// Seconds to hours, minutes & seconds
// V2: Format such as 02:07:09

using System;
public class Hours
{
    public static void Main()
    {
        int totalSeconds, hours, minutes, seconds;
        string data;
        bool end = false;
        while(!end)
        {
            Console.Write("How many seconds? ");
            data = Console.ReadLine();
            if(data == "")
                end = true;
            else
            {
                
                totalSeconds = Convert.ToInt32(data);
                
                hours = totalSeconds / 3600;
                minutes = (totalSeconds % 3600) / 60;
                seconds = (totalSeconds % 3600) % 60;
                
                
                Console.Write(hours.ToString("00") + ":");
                Console.Write(minutes.ToString("00" + ":"));
                Console.WriteLine(seconds.ToString("00"));
                
                Console.WriteLine("{0}:{1}:{2}",
                    hours.ToString("00"),
                    minutes.ToString("00"),
                    seconds.ToString("00")
                    );
            }
        }
        Console.WriteLine("Bye!");
        
    }
}
