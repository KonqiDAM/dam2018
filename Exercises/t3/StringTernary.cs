//Ivan Lazcano
using System;
class MainClass
{
    public static void Main()
    {
        string textoComoNota;
        double nota;
        
        Console.Write("Introduce nota: ");
        nota = Convert.ToDouble(Console.ReadLine());
        
        if(nota >= 60)
            textoComoNota = "aprobado";
        else
            textoComoNota = "suspenso";
        
        Console.WriteLine("Estas: " + textoComoNota);
        
        textoComoNota = 60 > nota ? "suspenso" : "aprobado";
        
        Console.WriteLine("Estas: " + textoComoNota);
        //Console.WriteLine("Estas: " + (60 > nota ? "suspenso" : "aprobado"));
    }
}
