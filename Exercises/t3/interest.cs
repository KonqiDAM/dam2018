using System;

public class Cifrado
{
    public static void Main()
    {
        decimal final = 1;
        Console.Write("Enter years: ");
        decimal years = Convert.ToDecimal(Console.ReadLine());
        Console.Write("Enter capital: ");
        decimal capital = Convert.ToDecimal(Console.ReadLine());
        Console.Write("Enter interest: ");
        decimal interest = Convert.ToDecimal(Console.ReadLine());


        for (int i = 1; i <= years; i++)
        {
            final = final * (1 + interest);
        }

        final = capital * final;

        Console.WriteLine(final/100);

    }
}
