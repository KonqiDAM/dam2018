//Ivan Lazcano
using System;
public class Trapecio
{
    public static void Main()
    {
        byte spaces;
        Console.Write("Enter width: ");
        byte width = Convert.ToByte(Console.ReadLine());
        Console.Write("Enter height: ");
        byte  height = Convert.ToByte(Console.ReadLine());
        spaces = (byte) (height - 1);

        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < spaces; j++)
            {
                Console.Write(" ");
            }
            spaces--;
            for (int k = 0; k < width; k++)
            {
                Console.Write("*");
            }
            width += 2;
            Console.WriteLine();
        }
    }
}
