//Ivan Lazcano Sindin
using System;
public class OversizedArray
{
    public static void Main()
    {
        byte option;
        int currentPosition = 0;
        int newPosition;
        const byte SIZE = 100;
        string[] data = new string[SIZE];
        string temporalData;

        do
        {
            // Show Menu
            Console.Clear();
            Console.WriteLine("1. Add");
            Console.WriteLine("2. Show all");
            Console.WriteLine("3. Enter data between existing data");
            Console.WriteLine("4. Delete an entry");
            Console.WriteLine("0. Exit");
            option = Convert.ToByte(Console.ReadLine());

            // Option 1: Add
            if (option == 1)
            {
                if (currentPosition < SIZE)
                {
                    Console.WriteLine("Enter data for record {0}",
                        currentPosition + 1);
                    data[currentPosition] = Console.ReadLine();
                    currentPosition++;
                }
                else
                {
                    Console.WriteLine("Database full");
                    Console.WriteLine("Press a key to continue");
                    Console.ReadLine();
                }
            }
            // Option 2: Show all
            else if (option == 2)
            {
                Console.Clear();
                for (int i = 0; i < currentPosition; i++)
                {
                    Console.WriteLine((i + 1) + ". " + data[i]);
                }
                Console.WriteLine("Press a key to continue");
                Console.ReadLine();
            }
            //Option 3: add new in bettwen 2 existend data
            else if (option == 3)
            {
                if (currentPosition < SIZE)
                {

                    Console.Write("Insert new entry: ");
                    temporalData = Console.ReadLine();
                    Console.Write("Insert position: ");
                    newPosition = Convert.ToInt32(Console.ReadLine());
                    if (newPosition > currentPosition)
                    {
                        Console.WriteLine("Number to high");
                        Console.WriteLine("Press a key to continue");
                        Console.ReadLine();
                    }
                    else
                    {
                        for (int i = currentPosition; i >= newPosition - 1; i--)
                        {
                            data[i + 1] = data[i];
                        }
                        data[newPosition - 1] = temporalData;
                        currentPosition++;
                        Console.WriteLine("Press a key to continue");
                        Console.ReadLine();
                    }
                }
                else
                {
                    Console.WriteLine("Database full");
                    Console.WriteLine("Press a key to continue");
                    Console.ReadLine();
                }
            }
            else if(option == 4)
            {
                Console.Write("Insert position to delete: ");
                newPosition = Convert.ToInt32(Console.ReadLine());
                if ( (newPosition - 1) < currentPosition)
                {

                    Console.WriteLine("The entry {0} is going to be deleted, type 'yes' to continue", data[newPosition-1]);
                    if (Console.ReadLine() == "yes")
                    {
                        for (int i = newPosition - 1; i < currentPosition - 1; i++)
                        {
                            data[i] = data[i + 1];
                        }
                        data[currentPosition] = "";
                        currentPosition--;
                        Console.WriteLine("Entry deleted with exit. Press a key to continue");
                        Console.ReadLine();
                    }
                    else
                    {
                        Console.WriteLine("Operation aborted");
                        Console.WriteLine("Press a key to continue");
                        Console.ReadLine();
                    }
                }
                else
                {
                    Console.WriteLine("Error, the entry does not exist");
                    Console.WriteLine("Press a key to continue");
                    Console.ReadLine();
                }

            }
        }
        while (option != 0);
    }
}
