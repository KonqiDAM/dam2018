// Ivan Lazcano Sindin
using System;
class MainClass
{
    public static void Main(string[] args)
    {
        char[,] cuadricula = { { '.', '.', '.' },
                               { '.', '.', '.' },
                               { '.', '.', '.' } 
                             };
        int posx, posy;

        Console.Write("Enter row: ");
        posy = Convert.ToInt32(Console.ReadLine());
        Console.Write("Enter col: ");
        posx = Convert.ToInt32(Console.ReadLine());

        cuadricula[posy, posx] = 'O';

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                    Console.Write(cuadricula[i,j]);
            }
            Console.WriteLine();
        }
    }
}
