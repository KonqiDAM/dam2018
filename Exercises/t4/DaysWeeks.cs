//Ivan Lazcano Sindin
using System;

public class MainClass
{
    public static void Main()
    {
        string[] daysWeek = {"Monday", "Tuesday", "Wednesday", "Thursday",
            "Friday", "Saturday", "Sunday"};
            
        for (int i = 6; i >= 0; i--)
        {
            Console.Write(daysWeek[i] + " ");
        }

    }
}
