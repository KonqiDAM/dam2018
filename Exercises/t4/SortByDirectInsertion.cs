//Ivan Lazcano
//direct insertion

using System;

public class BubbleSort
{
    public static void Main()
    {
        const int SIZE = 5;
        int aux;

        // Let's ask for data

        int[] data = new int[SIZE];
        for (int i = 0; i < SIZE; i++)
        {
            Console.Write("Enter number {0}: ", i + 1);
            data[i] = Convert.ToInt32(Console.ReadLine());
        }

        // And display the array
        for (int z = 0; z < SIZE; z++)
        {
            Console.Write(data[z] + " ");
        }
        Console.WriteLine();


        // Now let's sort
        for (int i = 2; i < SIZE; i++)
        {
            int j = i - 1;
            while(j>= 1 && (data[j] > data[j+1]))
            {
                aux = data[i];
                data[i] = data[j];
                data[j] = aux;

            }
            j = j - 1;
        }
        for (int z = 0; z < SIZE; z++)
        {
            Console.Write(data[z] + " ");
        }
        Console.WriteLine();
    }
}
