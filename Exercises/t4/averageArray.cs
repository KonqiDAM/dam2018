//Ivan Lazcano Sindin
using System;

public class MainClass
{
    public static void Main()
    {
        int[] numbers = new int[6];
        int sum = 0;
        for (int i = 0; i < 6; i++)
        {
            Console.Write("Enter number: ");
            numbers[i] = Convert.ToInt32(Console.ReadLine());
        }

        for (int i = 0; i < 6; i++)
        {
            sum += numbers[i];
        }
        Console.WriteLine("Average = {0}", sum/6.0);
    }
}
