//Ivan Lazcano Sindin
using System;
public class JaggedArray
{
    public static void Main()
    {
        int[][] matrix = new int[3][];
        matrix[0] = new int[6];
        matrix[1] = new int[5];
        matrix[2] = new int[4];

        for (int j = 0; j < matrix.Length; j++)
        {
            for (int i = 0; i < matrix[j].Length; i++)
            {
                Console.WriteLine("Enter a number {0} of array {0}: ",
                                 i+1, j+1);
                matrix[j][i] = Convert.ToInt32(Console.ReadLine());
            }
        }

        for (int j = 0; j < matrix.Length; j++)
        {
            for (int i = 0; i < matrix[j].Length; i++)
            {
                Console.WriteLine("Position: {0}:{1}-> Value: {2} ", j + 1, i + 1,
                                  matrix[j][i]);
            }
        }

    }
}
