//Ivan Lazcano Sindin
using System;

public class OrdenadDatos
{


    public static void Main()
    {
        const int SIZE = 5;
        int[] numbers = new int[SIZE];
        for (int i = 0; i < numbers.Length; i++)
        {
            Console.WriteLine("Enter data {0}", i + 1);
            numbers[i] = Convert.ToInt32(Console.ReadLine());
        }


        for (int i = 0; i < numbers.Length; i++)
        {
            for (int j = 0; j < numbers.Length; j++)
            {
                if (numbers[j] > numbers[i])
                {

                    numbers[j] -= numbers[i];
                    numbers[i] += numbers[j];
                    numbers[j] = numbers[i] - numbers[j];
                }
            }
        }

        Console.WriteLine();
        for (int i = 0; i < numbers.Length; i++)
        {
            Console.Write(numbers[i] + " ");
        }
        Console.WriteLine();
        Console.WriteLine("Min: {0}, Max: {1}, Mid: {2}", numbers[0],
                          numbers[SIZE-1], numbers[SIZE/2]);
    }
}
