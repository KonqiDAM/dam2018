//Ivan Lazcano Sindin
using System;

public class MainClass
{
    public static void Main()
    {
        int count = 0;
        Console.Write("Enter some text: ");
        string text = Console.ReadLine();
        foreach (string t in text.Split())
        {
            count = 0;
            foreach (string t2 in text.Split())
            {
                if (t == t2)
                    count++;
            }
            if (count >= 2)
            {
                Console.WriteLine("There are repeated words");
                break;
            }
        }

    }
}
