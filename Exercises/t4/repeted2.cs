//Ivan Lazcano Sindin
using System;

public class MainClass
{
    public static void Main()
    {
        
        Console.Write("Enter some text: ");
        string[] text = Console.ReadLine().Split();

        for (int i = 0; i < text.Length; i++)
        {
            for (int j = i+1; j < text.Length; j++)
            {
                if (text[i] == text[j])
                    Console.WriteLine("Repeated word: " + text[i]);
            }
        }

    }
}
