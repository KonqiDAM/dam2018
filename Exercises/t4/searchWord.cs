//Ivan Lazcano Sindin
using System;

public class MainClass
{
    public static void Main()
    {
        int count = 0;
        Console.Write("Enter some text: ");
        string text = Console.ReadLine();
        Console.Write("Enter the word to search: ");
        string word = Console.ReadLine();

        foreach(string t in text.Split())
        {
            if (t == word)
                count++;
        }
        Console.WriteLine("Found the word {0} times", count);
    }
}
