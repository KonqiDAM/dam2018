//Ivan Lazcano Sindin
using System;

public class MainClass
{
    public static void Main()
    {
        string text = Console.ReadLine();

        foreach(string word in text.Split())
        {
            Console.WriteLine(word);
        }
    }
}
