//Ivan Lazcano Sindin
using System;

public class MainClass
{
    public static void Main()
    {
        int sum = 0;
        Console.Write("Enter some text: ");
        string text = Console.ReadLine();

        foreach(string number in text.Split())
        {
            sum += Convert.ToInt32(number);
        }
        Console.WriteLine("The sum is: " + sum);
    }
}
