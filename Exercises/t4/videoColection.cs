//Ivan Lazcano Sindin
using System;

public class VideoCollection
{
    struct video
    {
        public string title;
        public ushort duration;
        public uint size;

    }
    public static void Main()
    {
        int count = 0;
        video[] v = new video[1000];
        video t;
        string option;
        string search;
        do
        {
            Console.WriteLine("1. Add new video");
            Console.WriteLine("2. Show all");
            Console.WriteLine("3. Search");
            Console.WriteLine("4. Sort by size");
            Console.WriteLine("0. Exit");
            option = Console.ReadLine();

            switch (option)
            {
                case "1":
                    Console.WriteLine("Name? ");
                    v[count].title = Console.ReadLine();
                    Console.WriteLine("Duration in seconds? ");
                    v[count].duration = Convert.ToUInt16(Console.ReadLine());
                    Console.WriteLine("Size in KB? ");
                    v[count].size = Convert.ToUInt32(Console.ReadLine());
                    count++;
                    break;
                case "2":
                    for (int i = 0; i < count; i++)
                    {
                        Console.WriteLine("Entry number: {0}", i+1);
                        Console.WriteLine("Title: {0}", v[i].title);
                        Console.WriteLine("Duration (seconds): {0}", 
                                          v[i].duration);
                        Console.WriteLine("Size (kb): {0}", v[i].size);
                        Console.WriteLine();
                    }
                    break;
                case "3":
                    Console.WriteLine("Search term? ");
                    search = Console.ReadLine();
                    for (int i = 0; i < count; i++)
                    {
                        if(v[i].title.Contains(search))
                        {
                            Console.WriteLine("Entry number: {0}", i + 1);
                            Console.WriteLine("Title: {0}", v[i].title);
                            Console.WriteLine("Duration (seconds): {0}",
                                              v[i].duration);
                            Console.WriteLine("Size (kb): {0}", v[i].size);
                            Console.WriteLine();
                        }
                    }
                    break;
                case "4":
                    for (int i = 0; i < count; i++)
                    {
                        for (int j = 0; j < count; j++)
                        {
                            if (v[i].size < v[j].size)
                            {
                                t = v[j];
                                v[j] = v[i];
                                v[i] = t;
                            }
                        }
                    }
                    break;

            }
        } while (option != "0");
    }
}
