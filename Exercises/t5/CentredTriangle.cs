//Ivan Lazcano Sindin

using System;
public class CentredTriangle
{
    public static void WriteCentredTriangle (char c, int n)
    {
        int spaces = 0, chars = n;
        string spaceLine = new string(' ', 40 - n / 2);
        for (; chars > 0; spaces++, chars -= 2)
        {
            Console.Write(spaceLine);
            for (int j = 0; j < spaces; j++)
            {
                Console.Write(' ');
            }
            for (int j = 0; j < chars; j++)
            {
                Console.Write(c);
            }
            Console.WriteLine();
        }

    }

    public static void Main()
    {
        WriteCentredTriangle('#', 13);
        WriteCentredTriangle('*', 3);
        WriteCentredTriangle('2', 20);
    }
}
