//Ivan Lazcano Sindin

using System;
public class InicialesFun
{
    public static string Iniciales (string texto)
    {
        string devolver = texto[0] + "";
        for (int i = 1; i < texto.Length; i++)
        {
            if (texto[i - 1] == ' ')
                devolver += texto[i];
        }

        return devolver.ToUpper();
    }

    public static void Main()
    {
        Console.WriteLine(Iniciales("asd dd ww gg"));
    }
}
