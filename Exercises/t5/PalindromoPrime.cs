//Ivan Lazcano
using System;

public class Yee
{
    public static bool IsPalindromo(string input)
    {
        for (int i = 0, j = input.Length - 1; i < input.Length / 2; i++, j--)
        {
            if (input[i] != input[j])
                return false;
        }
        return true;
    }
    public static bool IsPrime(long number)
    {
        for (int i = 2; i < number / 2; i++)
        {
            if (number % i == 0)
                return false;
        }
        return true;
    }
    public static bool IsPalindromoAndPrime(long number)
    {

        return IsPalindromo(number.ToString()) && IsPrime(number);

    }
    public static void Main()
    {
        DateTime inicio;


        int count = 0;
        long number = Convert.ToInt64(Console.ReadLine());
        long number2 = Convert.ToInt64(Console.ReadLine());
        if (number2 < number)
        {
            long swap = number;
            number = number2;
            number2 = swap;
        }
        inicio = DateTime.Now;
        for (long i = number; i <= number2; i++)
        {
            if (IsPalindromoAndPrime(i))
                count++;
        }
        Console.WriteLine(count);
        Console.WriteLine(DateTime.Now-inicio);

    }
}
