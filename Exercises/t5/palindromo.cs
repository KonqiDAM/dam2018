//Ivan Lazcano
using System;

public class Yee
{
    public static bool IsPalindromo(string input)
    {
        for (int i = 0, j = input.Length-1; i < input.Length / 2; i++, j--)
        {
            if (input[i] != input[j])
                return false;
        }
        return true;
    }
    public static void Main()
    {
        Console.WriteLine(IsPalindromo("939999999")); 
    }
}
