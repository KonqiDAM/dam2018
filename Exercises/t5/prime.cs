//Ivan Lazcano
using System;

public class Yee
{
    public static bool IsPrime(long number)
    {
        for (int i = 2; i < number/2; i++)
        {
            if(number % i == 0)
                return false;
        }
        return true;
    }
    public static void Main()
    {
        Console.WriteLine(IsPrime(100));
    }
}
