//Ivan Lazcano
using System;

public class Yee
{
    public static int Sum(int[] numbers)
    {
        int sum = 0;
        for (int i = 0; i < numbers.Length; i++)
        {
            sum += numbers[i];
        }
        return sum;
    }
    
    public static void Main()
    {
        int[] n = { 3, 4, 2 };
        Console.WriteLine("La suma del array de ejemplo es: " + Sum(n)); 
    }
}
