//Ivan Lazcano Sindin
using System;
public class Titulo
{
    public static bool EsAnyoBisiesto(int ano)
    {
        return ano % 4 == 0 && ano % 100 != 0 || ano % 400 == 0;
    }
    public static void Main()
    {
        Console.WriteLine(EsAnyoBisiesto(1984));
    }
}
