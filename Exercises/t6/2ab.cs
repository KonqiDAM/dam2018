//Ivan Lazcano Sindin
using System;
public class DibujarRectanguloHuecoPrograma
{
    public static void DibujarRectanguloHueco(int ancho, int alto, char c)
    {
        Console.WriteLine(new string(c,ancho));
        string hueco = c + new string(' ', ancho - 2) + c;
        for (int i = 0; i < alto-2; i++)
        {
            Console.WriteLine(hueco);
        }
        Console.WriteLine(new string(c, ancho));
    }
    public static void Main()
    {

        DibujarRectanguloHueco(11,5,'*');
    }
}
