//Ivan Lazcano Sindin
using System;
public class Contar
{
    public static void ContarVE(string s, ref int v, ref int e)
    {
        foreach(char c in s.ToLower())
        {
            if (c == ' ')
                e++;
            else if (c == 'a' || c == 'e' || c == 'i' || c == 'o' ||
                   c == 'u')
                v++;
        }
    }
    public static void Main()
    {
        int v = 0, e = 0;
        ContarVE("Esta es una frase",ref v, ref e);
        Console.WriteLine(e + " " + v);
    }
}
