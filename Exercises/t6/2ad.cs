//Ivan Lazcano Sindin
using System;
public class Multiplicaciones
{
    public static int Multiplicar(int n, int p)
    {
        int r = 0;
        for (int i = 0; i < p; i++)
        {
            r += n;
        }
        return r;
    }

    public static int MultiplicarR(int n, int p)
    {
        if (p == 1)
            return n;
        return n + MultiplicarR(n, p -1);
    }
    public static void Main()
    {
        Console.WriteLine(Multiplicar(5, 13));
    }
}
