//Ivan Lazcano Sindin
using System;
public class Calculadora
{


    public static int MultiplicarR(int n, int p)
    {
        if (p == 1)
            return n;
        return n + MultiplicarR(n, p -1);
    }
    public static void Main(string[] args)
    {
        if (args.Length != 3)
            Console.WriteLine("Debe recibir 3 arumentos, un numero, " +
                              "operador y otro numero");
        else
        {
            if (args[1] == "*")
                Console.WriteLine(MultiplicarR(Convert.ToInt32(args[0]),
                                               Convert.ToInt32(args[2])));
            else if (args[1] == "-")
                Console.WriteLine(Convert.ToInt32(args[0]) -
                                  Convert.ToInt32(args[2]));
            else if (args[1] == "+")
                Console.WriteLine(Convert.ToInt32(args[0]) +
                                  Convert.ToInt32(args[2]));
            else if (args[1] == "/")
                Console.WriteLine(Convert.ToInt32(args[0]) /
                                  Convert.ToInt32(args[2]));
            else
            {
                Console.WriteLine("Operador desconocido. Operadores " +
                                  "validos: (+, -, *, /)");
            }
        }
    }
}
