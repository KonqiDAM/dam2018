//Ivan Lazcano Sindin
using System;
public class MostrarTextoRecuadradoPrograma
{
    public static void MostrarTextoRecuadrado(string s)
    {
        Console.WriteLine("+" + new string('-', s.Length+4) + "+");
        Console.WriteLine("|  " + s + "  |");
        Console.WriteLine("+" + new string('-', s.Length + 4) + "+");
    }
    public static void Main(string[] args)
    {
        MostrarTextoRecuadrado("Hola Mundo Cruel!");
    }
}
