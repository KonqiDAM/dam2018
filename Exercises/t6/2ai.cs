//Ivan Lazcano Sindin
using System;
public class MostrarTextoRecuadradoPrograma
{
    public static int CantidadDeDigitos(int n)
    {
        int d = 0;
        while(n > 0)
        {
            n /= 10;
            d++;

        }
        return d;
    }
    public static int CantidadDeDigitosR(int n)
    {
        if (n < 10)
            return 1;
        return 1 + CantidadDeDigitosR(n / 10);
    }
    public static void Main(string[] args)
    {
        Console.WriteLine(CantidadDeDigitos(666)); // Debería mostrar 3
        Console.WriteLine(CantidadDeDigitosR(99999));
        if (CantidadDeDigitosR(1005) != 4)
            Console.WriteLine("Falla!");

    }
}
