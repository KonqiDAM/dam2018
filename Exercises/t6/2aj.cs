//Ivan Lazcano Sindin
using System;
public class EsEmirpPrograma
{
    public static bool EsPrimo(int n)
    {
        for (int i = 2; i < n/2; i++)
        {
            if (n % i == 0)
                return false;
        }
        return true;
    }
    public static bool EsEmirp(int n)
    {
        if(EsPrimo(n))
        {
            char[] array = n.ToString().ToCharArray();
            Array.Reverse(array);
            n = Convert.ToInt32(new String(array));
            if (EsPrimo(n))
                return true;
        }

        return false;
    }
    public static void Main(string[] args)
    {
        Console.WriteLine(EsPrimo(17));
        Console.WriteLine(EsPrimo(13));
        Console.WriteLine(EsPrimo(37));
        Console.WriteLine(EsPrimo(79));
        Console.WriteLine(EsPrimo(12)); 
        Console.WriteLine(EsPrimo(40));
        Console.WriteLine(EsPrimo(7));
    }
}
