//Ivan Lazcano Sindin
using System;
public class Raizes
{
    public static void ResolverSegundoGrado
            (int a, int b, int c, ref double x1, ref double x2)
    {
        if( b * b - (4 * a * c) < 0)
        {
            x2 = x1 = -9999;
            return;
        }
        double r1 = Math.Sqrt(r1 = b * b - (4 * a * c));

        try
        {
            x1 = (-b - r1) / 2 * a;
        } catch (Exception) {
            x1 = -9999;
        }
        try
        {
            x2 = ( -b + r1 ) / 2 * a;
        }
        catch (Exception)
        {
            x2 = -9999;
        }
    }


    public static void Main(string[] args)
    {
        int a = 1, b = 0, c = -1;
        double x1 = 0, x2 = 0;
        ResolverSegundoGrado(a, b, c, ref x1, ref x2);
        Console.WriteLine("Las soluciones son " + x1 + " y " + x2); // -1, 1
    }
}
