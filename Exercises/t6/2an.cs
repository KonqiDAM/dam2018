//Ivan Lazcano Sindin
using System;
public class MostrarTextoRecuadradoPrograma
{
    public static void Obtener2Max(float[] d, ref float max, ref float segundo)
    {
        int posMax = 0, posSegundo = 0;
        for (int i = 0; i < d.Length; i++)
        {
            if (d[i] > d[posMax])
                posMax = i;
        }
        for (int i = 0; i < d.Length; i++)
        {
            if (d[i] >= d[posSegundo] && posMax != i)
                posSegundo = i;
        }

        max = d[posMax];
        segundo = d[posSegundo];
    }
    public static void Main(string[] args)
    {
        float max = 0, segundo = 0;
        float[] data = { 2, 7.5f, 6, -1, 20, 5 };

        Obtener2Max(data, ref max, ref segundo);

        Console.WriteLine("El máximo es " + max + ", el segundo es " + segundo);

    }
}
