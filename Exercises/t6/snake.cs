//Ivan Lazcano Sindin
using System;
public class Snake
{
    static Random rnd;
    static ConsoleKeyInfo key;

    public static void ReadLevel(ref char[,] map, int lvl)
    {
        string[] file = System.IO.File.ReadAllText(
            "level" + lvl + ".map").Split('\n');
        map = new char[file.Length-1, file[0].Length];
        for (int i = 0; i < file.Length-1; i++)
        {
            for (int j = 0; j < file[i].Length; j++)
            {
                map[i, j] = file[i][j];
                Console.Write(map[i, j]);
            }
            Console.WriteLine();
        }
    }

    public static void PrintMap(char[,] map)
    {
        Console.Clear();

        for (int i = 0; i < map.GetLength(0); i++)
        {
            for (int j = 0; j < map.GetLength(1); j++)
            {
                Console.Write(map[i,j]);
            }
            Console.WriteLine();
        }

    }

    public static void PrintfStats(int p, int l)
    {
        Console.WriteLine("Puntuacion: " + p);
        Console.WriteLine("Longitud serpiente: " + l);
    }

    public static void PutObstacles(ref char[,] map, int cant)
    {
        
        int x, y;
        for (int i = 0; i < cant; i++)
        {
            do
            {
                y = rnd.Next(1, map.GetLength(0) - 1);
                x = rnd.Next(1, map.GetLength(1) - 1);
            } while (map[y, x] != ' ');
            map[y, x] = 'x';
        }
    }

    public static void PutObjetive(ref char[,] map, int cant)
    {

        int x, y;
        for (int i = 0; i < cant; i++)
        {
            do
            {
                y = rnd.Next(1, map.GetLength(0) - 1);
                x = rnd.Next(1, map.GetLength(1) - 1);
            } while (map[y, x] != ' ');
            map[y, x] = 'o';
        }
    }

    public static void StartMap(ref char[,] map, ref int x, ref int y, int pac, int obs)
    {
        int midx = map.GetLength(1) / 2;
        int midy = map.GetLength(0) / 2;
        PutObstacles(ref map, obs);
        PutObjetive(ref map, pac);
        map[midy, midx] = 'S';
        y = midy;x = midx; 
    }

    public static int ReadDirection()
    {
        if (Console.KeyAvailable)
        {
            key = Console.ReadKey();
            if (key.Key == ConsoleKey.LeftArrow)
                return 4;
            if (key.Key == ConsoleKey.RightArrow)
                return 2;
            if (key.Key == ConsoleKey.UpArrow)
                return 1;
            if (key.Key == ConsoleKey.DownArrow)
                return 3;
            if (key.Key == ConsoleKey.Escape)
                return -1;
        }
        return 0;
    }


    public static bool MoveSnake(ref char[,] map, int dir, 
                                 ref int lastDir, ref int[,] pos,
                                 ref int p, ref int lon,
                                 ref int awards)
    {
        int x = 0, y = 0;
        if (dir == 0)
            dir = lastDir;
        //if (lastDir == 0)
            //return true;
        switch (dir)
        {
            case 1:
                y--;
                break;
            case 2:
                x++;
                break;
            case 3:
                y++;
                break;
            case 4:
                x--;
                break;
        }
        if (pos[1,0] + y == pos[1,1] && pos[0,0] + x == pos[0,1])
            MoveSnake(ref map, lastDir, ref lastDir, ref pos, ref p,
                      ref lon, ref awards);
        else
        {
            if (map[pos[1,0] + y, pos[0,0] + x] == 'x' || map[pos[1,0] + y, pos[0,0] + x] == 's')
                return false;
            else if (map[pos[1,0] + y, pos[0,0] + x] == 'o')
            {
                p += 10;
                lon++;
                pos[0, lon] = pos[0, lon - 1];
                pos[1, lon] = pos[1, lon - 1];
                awards--;
            }

            MoveTail(ref map, ref pos, lon);
            pos[1, 0] += y;
            pos[0, 0] += x;
            map[pos[1, 0], pos[0, 0]] = 'S';
            lastDir = dir;
            return true;
        }
        return true;
    }

    public static void MoveTail(ref char[,] map, ref int[,] pos, int lon)
    {
        map[pos[1, lon], pos[0, lon]] = ' ';
        for (int i = lon; i > 0; i--)
        {
            pos[0, i] = pos[0, i - 1];
            pos[1, i] = pos[1, i - 1];
            map[pos[1, i], pos[0, i]] = 's';
            //Se podria optimizar mucho
        }
    }

    public static void Winer()
    {
        Console.Clear();
        Console.WriteLine("Enhorabuena has ganado!!!");
        Environment.Exit(0);
    }

    public static void Main()
    {
        const int CANTLVLS = 3;
        const int MAXLONG = 30;
        const int PAC = 20;
        const int OBS = 10;
        int speed = 350;
        int lon = 0;
        int premios = PAC;
        int[,] pos = new int[MAXLONG, MAXLONG];
        int actualLevel = 1;
        pos[0, 0] = 0;
        pos[1, 0] = 0;
        rnd = new Random();
        char[,] map = new char[1,1];
        int lastDir = 0;
        int points = 0;
        ReadLevel(ref map, actualLevel);
        StartMap(ref map, ref pos[0, 0], ref pos[1, 0], PAC, OBS);
        do
        {
            if(PAC / 2 == premios )
            {
                PutObstacles(ref map, OBS);
                //Lo malo es que te puede salir un obstaculo justamente 
                //delante, pero simplemente esperemos que no :P
                PutObjetive(ref map, PAC);
                premios += PAC;
                speed -= 50;
            }
            if(lon >= MAXLONG-1)
            {
                lon = 0;
                pos[0, 0] = 0;
                pos[1, 0] = 0;
                lastDir = 0;
                actualLevel++;
                if (CANTLVLS < actualLevel)
                    Winer();
                ReadLevel(ref map, actualLevel);
                StartMap(ref map, ref pos[0, 0], ref pos[1, 0], PAC, OBS);
                premios = PAC;
                points += 50;
            }
            PrintMap(map);
            PrintfStats(points, lon);
            System.Threading.Thread.Sleep(speed);
        } while (MoveSnake(ref map, ReadDirection(), ref lastDir, 
                           ref pos, ref points, ref lon, ref premios));
        Console.WriteLine("Perdiste!!!");
    }
}
