//Ivan Lazcano Sindin
using System;
class Documento
{
    protected string titulo;
    protected string autor;
    protected int numeroPaginas;

    public Documento( string t, string a, int np)
    {
        titulo = t;
        autor = a;
        numeroPaginas = np;
    }
    public Documento()
    {
        titulo = "";
        autor = "";
        numeroPaginas = 0;
    }

    public string GetTitulo() { return titulo; }
    public string GetAutor() { return autor; }
    public int GetNumeroPaginas() { return numeroPaginas; }

    public void SetTitulo(string t) { titulo = t; }
    public void SetAutor(string a) { autor = a; }
    public void SetNumeroPaginas(int np) { numeroPaginas = np; }

}
//-------------------
class Test
{
    public static void Main()
    {
        Documento d = new Documento("Yee", "Nope", 5);
        Console.WriteLine("Paginas: " +  d.GetNumeroPaginas());
    }
}
