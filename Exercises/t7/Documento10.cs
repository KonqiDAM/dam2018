//Ivan Lazcano Sindin
using System;
class Documento
{
    protected string titulo;
    protected string autor;
    protected int numeroPaginas;

    public Documento( string titulo, string autor, int numeroPaginas)
    {
        this.titulo = titulo;
        this.autor = autor;
        this.numeroPaginas = numeroPaginas;
    }

    public Documento(string titulo)  : this(titulo, "Anon", 0)
    {
        
    }

    public string GetTitulo() { return titulo; }
    public string GetAutor() { return autor; }
    public int GetNumeroPaginas() { return numeroPaginas; }

    public void SetTitulo(string titulo) { this.titulo = titulo; }
    public void SetAutor(string autor) { this.autor = autor; }
    public void SetNumeroPaginas(int numeroPaginas) 
    { 
        this.numeroPaginas = numeroPaginas; 
    }

    public void MostrarDatos()
    {
        Console.Write("Autor: {0}, Titulo: {1}, Paginas: {2}", 
                          autor, titulo, numeroPaginas);
        
    }
}

//-----------------------------
class Libro : Documento
{
    char tapa;

    public Libro( string titulo, string autor, int numeroPaginas, char tapa)
        : base(titulo, autor, numeroPaginas)
    {
        this.tapa = tapa;

    }

    public char GetTapa() { return tapa; }
    public void SetTapa(char tapa) { this.tapa = tapa; }

    public new void MostrarDatos()
    {
        base.MostrarDatos();
        Console.WriteLine(", Cubierta: {0}", tapa);
    }

}
//-------------------------------
class Test
{
    public static void Main()
    {
        Documento[] d = new Documento[3];
        d[0] = new Documento("Doc1", "a1", 5);
        d[1] = new Documento("Doc2", "a2", 5);
        d[2] = new Libro("Libro3", "A3", 5, 'D');
        for (int i = 0; i < d.Length; i++)
        {
            d[i].MostrarDatos();
            Console.WriteLine();
        }
    }
}
