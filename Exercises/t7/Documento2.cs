//Ivan Lazcano Sindin
using System;
class Documento
{
    protected string titulo;
    protected string autor;
    protected int numeroPaginas;

    public Documento( string titulo, string autor, int numeroPaginas)
    {
        this.titulo = titulo;
        this.autor = autor;
        this.numeroPaginas = numeroPaginas;
    }
    public Documento()
    {
        titulo = "";
        autor = "";
        numeroPaginas = 0;
    }

    public string GetTitulo() { return titulo; }
    public string GetAutor() { return autor; }
    public int GetNumeroPaginas() { return numeroPaginas; }

    public void SetTitulo(string titulo) { this.titulo = titulo; }
    public void SetAutor(string autor) { this.autor = autor; }
    public void SetNumeroPaginas(int numeroPaginas) { this.numeroPaginas = numeroPaginas; }

}
//-------------------
class Test
{
    public static void Main()
    {
        Documento d = new Documento("Yee", "Nope", 5);
        Console.WriteLine("Paginas: " +  d.GetNumeroPaginas());
    }
}
