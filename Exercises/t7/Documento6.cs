//Ivan Lazcano Sindin
using System;
class Documento
{
    protected string titulo;
    protected string autor;
    protected int numeroPaginas;

    public Documento()
    {
        
    }

    public Documento( string titulo, string autor, int numeroPaginas)
    {
        this.titulo = titulo;
        this.autor = autor;
        this.numeroPaginas = numeroPaginas;
    }

    public Documento(string titulo)  : this(titulo, "Anon", 0)
    {
        
    }

    public string GetTitulo() { return titulo; }
    public string GetAutor() { return autor; }
    public int GetNumeroPaginas() { return numeroPaginas; }

    public void SetTitulo(string titulo) { this.titulo = titulo; }
    public void SetAutor(string autor) { this.autor = autor; }
    public void SetNumeroPaginas(int numeroPaginas) { this.numeroPaginas = numeroPaginas; }

}

//-----------------------------
class Libro : Documento
{
    char tapa;

    public Libro(char tapa, string titulo, string autor, int numeroPaginas)
    {
        this.tapa = tapa;
        this.titulo = titulo;
        this.autor = autor;
        this.numeroPaginas = numeroPaginas;
    }

    public char GetTapa() { return tapa; }
    public void SetTapa(char tapa) { this.tapa = tapa; }

}
//-------------------------------
class Test
{
    public static void Main()
    {
        Documento[] d = new Documento[3];
        for (int i = 0; i < d.Length; i++)
        {
            d[i] = new Documento("" + (i+1), i*20 + "" , i*3);
            //Console.WriteLine( d[i].GetAutor() + d[i].GetTitulo() + d[i].GetNumeroPaginas() ); 
        }
    }
}
