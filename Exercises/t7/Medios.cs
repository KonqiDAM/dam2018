// Ivan Lazcano

using System;

class Medio
{
    protected string autor;
    protected long tamanyo;
    protected string formato;


    public Medio(string autor, long tamanyo, string formato)
    {
        this.autor = autor;
        this.tamanyo = tamanyo;
        this.formato = formato;
    }

    public string GetAutor() { return autor; }
    public long GetTamanyo() { return tamanyo; }
    public string GetFormato() { return formato; }

    public void SetAutor(string autor) { this.autor = autor; }
    public void SetTamanyo(long tamanyo) { this.tamanyo = tamanyo; }
    public void SetFormato(string formato) { this.formato = formato; }

    public override string ToString()
    {
        return "Autor: : " + autor + ", Tamanyo: " + tamanyo + ", Formato: " + formato;
    }
}

class Imagen : Medio
{
    protected int ancho;
    protected int alto;

    public Imagen(string autor, long tamanyo, string formato, 
                  int alto, int ancho) : base(autor, tamanyo, formato)
    {
        this.ancho = ancho;
        this.alto = alto;
    }

    public int GetAncho() { return ancho; }
    public int GetAlto() { return alto; }

    public void SetAncho(int ancho) { this.ancho = ancho; }
    public void SetAlto(int alto) { this.alto = alto; }

    public override string ToString()
    {
        return base.ToString() + formato + ", Alto: " + alto + ", Ancho: " + ancho;
    }
}

class Sonido : Medio
{
    protected bool estereo;
    protected long kbps;
    protected long duracion;

    public Sonido(string autor, long tamanyo, string formato,
                  bool estereo, long kbps, long duracion) 
        : base(autor, tamanyo, formato)
    {
        this.estereo = estereo;
        this.kbps = kbps;
        this.duracion = duracion;
    }

    public bool GetEstereo() { return estereo; }
    public long GetKbps() { return kbps; }
    public long GetdDuracion() { return duracion; }


    public void SetEstereo(bool estereo) { this.estereo = estereo; }
    public void SetKbps(long kbps) { this.kbps = kbps; }
    public void SetDuracion(long duracion) { this.duracion = duracion; }

    public override string ToString()
    {
        return base.ToString() + ", Setero: " + estereo 
                   + ", Kpbs: " + kbps + ", Duracion: " +duracion;
    }
}

class Video : Medio 
{

    protected string codec;
    protected int ancho;
    protected int alto;
    protected long duracion;

    public Video(string autor, long tamanyo, string formato, string codec,
                  int alto, int ancho, long duracion) : base(autor, tamanyo, formato)
    {
        this.ancho = ancho;
        this.alto = alto;
        this.duracion = duracion;
        this.codec = codec;
    }

    public string GetCodec() { return codec; }
    public int GetAncho() { return ancho; }
    public int GetAlto() { return alto; }
    public long GetDuracion() { return duracion; }

    public void SetCodec(string codec) { this.codec = codec; }
    public void SetAncho(int ancho) { this.ancho = ancho; }
    public void SetAlto(int alto) { this.alto = alto; }
    public void SetDuracion(long duracion) { this.duracion = duracion; }

    public override string ToString()
    {
        return base.ToString() + "Ancho: " + ancho + ", Alto: " + alto 
                   + ", Duracion: " + duracion + ", Codec: " + codec;
    }
}

class PruebaDeMedios
{
    public static void Main()
    {


        Medio m = new Medio("No one", 200, "avi");
        Imagen i = new Imagen("Ivan", 300, "mp4", 600, 800);
        Sonido s = new Sonido("A", 11, "wav", true, 200, 300);
        Video v = new Video("B", 200, "mkv", "h264", 200, 300, 100);

        Medio[] arr = new Medio[3];
        arr[0] = new Imagen("Ivan", 300, "mp4", 600, 800);
        arr[1] = new Sonido("A", 11, "wav", true, 200, 300);
        arr[2] = new Video("B", 200, "mkv", "h264", 200, 300, 100);

        foreach(Medio med in arr)
        {
            Console.WriteLine(med);
        }
    }
}
