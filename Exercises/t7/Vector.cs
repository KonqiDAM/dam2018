// Ivan Lazcano

using System;

class Vector2D
{
    private double x;
    private double y;

    public Vector2D( double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    public void SetX(double x) { this.x = x; }
    public void SetY(double y) { this.y = y; }

    public double GetX() { return x; }
    public double GetY() { return y; }

    public override string ToString()
    {
        return "<" + x + ", " + y + ">";
    }

    public double GetLength()
    {
        return Math.Sqrt(x * x + y * y);
    }

    public void Add(Vector2D v)
    {
        this.x += v.x;
        this.y += v.y;
    }

    public static Vector2D Sum(Vector2D v1, Vector2D v2)
    {
        return new Vector2D(v1.x + v2.x, v2.y + v2.y);
    }

    public static Vector2D operator +(Vector2D v1, Vector2D v2)
    {
        return new Vector2D(v1.x + v2.x, v2.y + v2.y);
    }
}

class TestVectors
{
    public static void Main()
    {
        Vector2D v = new Vector2D(20, 11);
        Console.WriteLine(v);
        v.Add(new Vector2D(12, 11));
        Console.WriteLine(v);
        Console.WriteLine(v.GetLength());
        Vector2D v1 = new Vector2D(11, 44);
        Vector2D v2 = new Vector2D(44, -4.2);

        Vector2D v3 = v1 + v2;
        v3 += v2;
        Console.WriteLine(v3);
    }
}
