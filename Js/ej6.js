"use strict";
let array = 
        [
            [ 3, 5 , 3, 1, 2, 4, 8, 10],
            [ 5, 6 , 2, 6, 8, 9, 0, 22],
            [ 1, 2 , 3, 4, 5, 6, 7, 8],
            [ 12, 32 , 55, 66, 77, 88, 99, 21]
        ];
for(let i = 0; i < array.length; i++)
{
    console.log("Total: " + array[i].reduce((total, num) => total + num, 0));
    console.log("Max: " + array[i].reduce((max, num) => num > max ? num : max, 0));
    console.log("Min: " + array[i].reduce((min, num) => num < min ? num : min, array[i][0]));
}
