"use strict";

let variable1="esto es un ejemplo de funciones de texto";
let variable2="utilizaremos varias de las funciones del documento";
let variable3="HAY QUE TENER CUIDADO CON LAS MAYÚSCULAS Y MINÚSCULAS"; 

console.log(variable1.toUpperCase());
console.log("Numero de caracters: " + variable1.length);
console.log(variable1 + " " + variable2);
console.log(variable1 + "en js");
console.log(variable3.toLowerCase());
console.log("var1  pos 5 = " + variable1[5]);
console.log("Primera 'j' en var1 = " + variable1.indexOf("j"))
console.log("Una porcion de var2 = " +  variable2.split(" ")[1])

let arr1 = ['u', 't', 'i' , 'l', 'z,', 'a', 'r', 'e', 'm', 'o','s'];
let arr2 = ["Utilizaremos", "varias", "de", "las", "funciones"];
console.log(arr1);
console.log(arr2);
console.log("Probando la posicion 5 de arr: " + arr1[5]);