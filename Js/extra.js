"use strict";
let n1 = prompt("Primer numero");
let n2 = prompt("Segundo numero");
let tipo = prompt("LISTA o TABLA?")
n1 = parseInt(n1);
n2 = parseInt(n2);
if(tipo != "LISTA" && tipo != "TABLA" || n1 < 1 || n1 > 10 || n2 < 1 || n2 > 10)
    document.write("<p>Error</p>");
else
{
    
    if(tipo == "LISTA")
    {
        document.write("<ul>");
        for (let i = 1; i < n2; i++) 
        {
            document.write("<li>" + n1 + "x" + i + "=" + (n1*i) + "</li>");
        }
        document.write("</ul>");
    }
    else
    {
        document.write('<table>');
        for (let i = 1; i < n2; i++) 
        {
            document.write("<tr>");
            document.write("<th>" + n1 + "x" + i + "</th>");
            document.write("<th>=</th>");
            document.write("<th>" + (n1*i) + "</th>");
            document.write("</tr>");
        }
        document.write("</table>");
    }
}
